﻿﻿﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.Content.BibleSearchMatch
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using MCL;
using System.Text.RegularExpressions;

namespace CMSMgmt.Content
{
  public class BibleSearchMatch
  {
    public string ID { get; private set; }

    public int Chapter { get; private set; }

    public int Verse { get; private set; }

    public string Text { get; private set; }

    public BibleSearchMatch(GroupCollection groups, string text)
    {
      this.ID = groups["id"].Value;
      this.Chapter = DTU.atoi(groups["ch"].Value);
      this.Verse = DTU.atoi(groups["vr"].Value);
      this.Text = text;
    }
  }
}
