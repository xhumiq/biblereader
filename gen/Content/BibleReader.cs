﻿﻿﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.Content.BibleReader
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using MCL;
using MCL.SysMgmt;
using MCL.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace CMSMgmt.Content
{
  public class BibleReader
  {
    private static Regex REGBText = new Regex("^(?:(?<pf>\\d)\\s*)?(?:(?<name>[A-Z][A-Z ]*)\\s*(?:(?<ch>\\d+)(?:\\s*\\:\\s*(?<vr>\\d+)?)?)?)?", RegexOptions.IgnoreCase);
    private static readonly Regex REG_OSISID = new Regex("^(?<id>[^\\s]+)\\s+(?<name>.*?)\\s*$");
    public static readonly Dictionary<string, string> ResByLang = new Dictionary<string, string>((IEqualityComparer<string>) StringComparer.CurrentCultureIgnoreCase);
    public static readonly Dictionary<string, string> BookIDs = new Dictionary<string, string>((IEqualityComparer<string>) StringComparer.CurrentCultureIgnoreCase);
    public static readonly Dictionary<string, string> ChiBookIDs = new Dictionary<string, string>((IEqualityComparer<string>) StringComparer.CurrentCultureIgnoreCase);
    private static string[] mBookNames = (string[]) null;
    private static string[] mChiBookNames = (string[]) null;
    private static string[] mSearchBookNames = (string[]) null;
    public static readonly SortedDictionary<string, string> mSearchBooksIDs = new SortedDictionary<string, string>((IComparer<string>) StringComparer.CurrentCultureIgnoreCase);
    public readonly SortedDictionary<string, BibleBook> Books = new SortedDictionary<string, BibleBook>((IComparer<string>) StringComparer.CurrentCultureIgnoreCase);
    public readonly SortedDictionary<string, BibleBook> BooksByName = new SortedDictionary<string, BibleBook>();

    public static string[] OSISIDs { get; private set; }

    public string Language { get; private set; }

    static BibleReader()
    {
      BibleReader.ResByLang.Add("eng", "@NKJV.xml");
      BibleReader.ResByLang.Add("chi", "@Chi-Union.xml");
      BibleReader.ReadOSISIDs();
    }

    public static void ReadOSISIDs()
    {
      StreamReader streamReader = new StreamReader(EmbResStore.GetEmbResStream<BibleReader>("OSISBookID.txt"));
      List<string> stringList1 = new List<string>();
      List<string> stringList2 = new List<string>();
      List<string> stringList3 = new List<string>();
      List<string> stringList4 = new List<string>();
      string str1;
      while ((str1 = streamReader.ReadLine()) != null)
      {
        if (!str1.StartsWith("---"))
        {
          string[] strArray = str1.Split(',');
          if (strArray.Length >= 3)
          {
            string str2 = strArray[1];
            BibleReader.BookIDs.Add(strArray[0], str2);
            BibleReader.ChiBookIDs.Add(strArray[0], strArray[2]);
            stringList1.Add(str2.ToUpper());
            stringList2.Add(strArray[2]);
            stringList3.Add(str2.ToUpper());
            BibleReader.mSearchBooksIDs.Add(str2.ToUpper(), strArray[0]);
            stringList4.Add(strArray[0]);
            if (char.IsDigit(str2[0]))
            {
              stringList3.Add(str2.Substring(2).ToUpper() + " " + (object) str2[0]);
              BibleReader.mSearchBooksIDs.Add(str2.Substring(2).ToUpper() + " " + (object) str2[0], strArray[0]);
              stringList3.Add(DTU.toRomanNum((short) DTU.atoi(str2[0].ToString())) + " " + str2.Substring(2).ToUpper());
              BibleReader.mSearchBooksIDs.Add(DTU.toRomanNum((short) DTU.atoi(str2[0].ToString())) + " " + str2.Substring(2).ToUpper(), strArray[0]);
            }
          }
        }
      }
      stringList1.Sort();
      BibleReader.mBookNames = stringList1.ToArray();
      BibleReader.mChiBookNames = stringList2.ToArray();
      stringList3.Sort();
      BibleReader.mSearchBookNames = stringList3.ToArray();
      BibleReader.OSISIDs = stringList4.ToArray();
    }

    public BibleBook[] NewTestimentBooks { get; private set; }

    public BibleBook[] OldTestimentBooks { get; private set; }

    public string Version { get; private set; }

    public DateTime VersionDate { get; private set; }

    public string FormatDescr { get; private set; }

    public void ReadOSIS(string fname)
    {
      using (Stream stream = (Stream) File.OpenRead(fname))
      {
        this.ReadOSIS(stream);
        stream.Close();
      }
    }

    public void ReadOSISRes(string resName) => this.ReadOSIS(EmbResStore.GetEmbResStream<BibleReader>(resName));

    public void ReadOSISLang(string language)
    {
      string fname = BibleReader.ResByLang.ContainsKey(language) ? BibleReader.ResByLang[language] : throw new SysException("Language: {0} not supported", new object[1]
      {
        (object) language
      });
      if (fname.StartsWith("@"))
        this.ReadOSIS(EmbResStore.GetEmbResStream<BibleReader>(fname.Substring(1)));
      else
        this.ReadOSIS(fname);
    }

    public void ReadOSIS(Stream stream) => this.ReadOSIS(new StreamReader(stream));

    public void ReadOSIS(StreamReader reader) => this.ReadOSIS((XmlReader) new XmlTextReader((TextReader) reader));

    public void ReadOSIS(XmlReader reader)
    {
      while (reader.Read())
      {
        if (reader.NodeType == XmlNodeType.Element && !(reader.LocalName != "osis"))
        {
          while (reader.Read())
          {
            if (reader.NodeType == XmlNodeType.Element && !(reader.LocalName != "osisText"))
            {
              while (reader.Read())
              {
                if (reader.NodeType == XmlNodeType.Element)
                {
                  switch (reader.LocalName)
                  {
                    case "header":
                      this.ReadOSISHeader(reader);
                      break;
                    case "div":
                      BibleBook bibleBook1 = new BibleBook();
                      bibleBook1.ReadOSISBook(reader);
                      if (this.Language == "chi")
                        bibleBook1.Name = BibleReader.ChiBookIDs[bibleBook1.ID];
                      this.Books.Add(bibleBook1.ID, bibleBook1);
                      this.BooksByName.Add(bibleBook1.Name.ToUpper(), bibleBook1);
                      if (char.IsDigit(bibleBook1.Name[0]))
                      {
                        this.BooksByName.Add(bibleBook1.Name.Substring(2).ToUpper() + " " + (object) bibleBook1.Name[0], bibleBook1);
                        this.BooksByName.Add(DTU.toRomanNum((short) DTU.atoi(bibleBook1.Name[0].ToString())) + " " + bibleBook1.Name.Substring(2).ToUpper(), bibleBook1);
                        break;
                      }
                      break;
                    default:
                      throw new SysException("Unknown xml element: {0}", new object[1]
                      {
                        (object) reader.LocalName
                      });
                  }
                }
              }
              List<BibleBook> bibleBookList = new List<BibleBook>();
              foreach (string osisiD in BibleReader.OSISIDs)
              {
                if (osisiD.ToUpper() == "MATT")
                {
                  this.OldTestimentBooks = bibleBookList.ToArray();
                  bibleBookList.Clear();
                }
                BibleBook bibleBook2 = (BibleBook) null;
                if (!this.Books.TryGetValue(osisiD, out bibleBook2))
                  throw new SysException("{0} doesn't contain {1}", new object[2]
                  {
                    (object) this.Version,
                    (object) osisiD
                  });
                bibleBookList.Add(bibleBook2);
              }
              this.NewTestimentBooks = bibleBookList.ToArray();
              break;
            }
          }
          break;
        }
      }
    }

    private void ReadOSISHeader(XmlReader reader)
    {
      while (reader.Read() && (reader.NodeType != XmlNodeType.EndElement || !(reader.LocalName == "header")))
      {
        if (reader.NodeType == XmlNodeType.Element)
        {
          switch (reader.LocalName)
          {
            case "revisionDesc":
              while (reader.Read() && (reader.NodeType != XmlNodeType.EndElement || !(reader.LocalName == "revisionDesc")))
              {
                if (reader.NodeType == XmlNodeType.Element)
                {
                  switch (reader.LocalName)
                  {
                    case "date":
                      this.VersionDate = DTU.toDate(reader.Value.Trim());
                      break;
                    case "p":
                      this.FormatDescr = reader.Value.Trim();
                      break;
                    default:
                      continue;
                  }
                }
              }
              break;
            case "work":
              while (reader.Read() && (reader.NodeType != XmlNodeType.EndElement || !(reader.LocalName == "work")))
              {
                if (reader.NodeType == XmlNodeType.Element)
                {
                  switch (reader.LocalName)
                  {
                    case "title":
                      reader.Read();
                      this.Version = reader.Value.Trim();
                      this.Language = this.Version.ToUpper().Contains("CHI") ? "chi" : "eng";
                      continue;
                    default:
                      continue;
                  }
                }
              }
              break;
            default:
              throw new SysException("Unknown xml element: {0}", new object[1]
              {
                (object) reader.LocalName
              });
          }
        }
      }
    }

    public BibleBook FindNearest(string name)
    {
      if (name == null || (name = name.Trim()).Length < 1)
        return (BibleBook) null;
      StringComparer ordinal = StringComparer.Ordinal;
      int index = 0;
      name = name.ToUpper();
      while (index < BibleReader.mBookNames.Length && ordinal.Compare(BibleReader.mBookNames[index], name) < 0)
        ++index;
      string mBookName;
      if (index < BibleReader.mBookNames.Length && (BibleReader.mBookNames[index].StartsWith(name.Substring(0, name.Length - 1)) || name.Length > 1 && BibleReader.mBookNames[index].StartsWith(name.Substring(0, name.Length - 1)) || name.Length == 1 && BibleReader.mBookNames[index].StartsWith(name)))
        mBookName = BibleReader.mBookNames[index];
      else if (index > 0 && (name.Length > 1 && BibleReader.mBookNames[index - 1].StartsWith(name.Substring(0, name.Length - 1)) || name.Length == 1 && BibleReader.mBookNames[index - 1].StartsWith(name)))
      {
        mBookName = BibleReader.mBookNames[index - 1];
      }
      else
      {
        TraceLogger.Send("Not found: {0} - {1}: {2}", (object) name, (object) index, (object) BibleReader.mBookNames[index]);
        return (BibleBook) null;
      }
      return this.BooksByName[mBookName];
    }

    public BibleBook[] FindBooks(string prefix)
    {
      if (prefix == null)
        return new BibleBook[0];
      if (prefix.Length < 1)
        return new BibleBook[0];
      prefix = prefix.ToUpper().Trim();
      List<BibleBook> bibleBookList = new List<BibleBook>();
      foreach (string mSearchBookName in BibleReader.mSearchBookNames)
      {
        if (mSearchBookName.StartsWith(prefix))
          bibleBookList.Add(this.Books[BibleReader.mSearchBooksIDs[mSearchBookName]]);
        if (mSearchBookName.StartsWith(prefix[0].ToString()))
          TraceLogger.Send("Finding: {0}", (object) mSearchBookName);
      }
      TraceLogger.Send("Find Book: {0} - {1}", (object) prefix, (object) bibleBookList.Count);
      return bibleBookList.ToArray();
    }

    public static void Test()
    {
      BibleReader br = new BibleReader();
      br.ReadOSISLang("eng");
      using (WinConsole winConsole = new WinConsole())
      {
        string text = (string) null;
        string name = (string) null;
        int chp = 1;
        int vr = 1;
        BibleBook prevBook = (BibleBook) null;
        winConsole.Run((Func<bool, Kernel32.CONSOLE_KEYSTATE, char, bool>) ((up, state, ch) =>
        {
          if (up)
            return false;
          int num1;
          switch (ch)
          {
            case '\b':
              num1 = text.Length <= 0 ? 1 : 0;
              break;
            case '\r':
              return true;
            default:
              num1 = 1;
              break;
          }
          if (num1 == 0)
            text = text.Substring(0, text.Length - 1);
          else if (char.IsLetterOrDigit(ch))
          {
            // ISSUE: reference to a compiler-generated field
            this.text += (string) (object) ch;
          }
          else if (ch == ' ' && (!text.EndsWith(" ") && text.Length > 0))
          {
            // ISSUE: reference to a compiler-generated field
            this.text += (string) (object) ch;
          }
          else
          {
            if (ch != ':' || text.EndsWith(":") || name == null || chp <= 0)
              return false;
            string str = text.TrimEnd();
            if (char.IsLetter(str[str.Length - 1]))
            {
              // ISSUE: reference to a compiler-generated field
              this.text += chp.ToString();
            }
            // ISSUE: reference to a compiler-generated field
            this.text += (string) (object) ch;
          }
          int key = chp;
          int num2 = vr;
          BibleBook bibleBook = (BibleBook) null;
          for (; text != null && text.Length > 0; text = ch.ToString())
          {
            Match match = BibleReader.REGBText.Match(text);
            if (match.Success)
            {
              name = (string) null;
              if (match.Groups["pf"].Value.Length > 0)
                text = name = match.Groups["pf"].Value + " ";
              if (match.Groups["name"].Value.Length > 0)
              {
                bibleBook = br.FindNearest(name.Trim());
                if (bibleBook != null && !bibleBook.Name.StartsWith(name))
                {
                  int length = name.Length;
                  if (bibleBook.Name.Length < length)
                    length = bibleBook.Name.Length;
                  name = bibleBook.Name.Substring(0, length);
                }
                // ISSUE: reference to a compiler-generated field
                text = (this.name += match.Groups["name"].Value.TrimEnd());
                if (match.Groups[nameof (ch)].Value.Length > 0)
                {
                  key = DTU.atoi(match.Groups[nameof (ch)].Value);
                  if (key <= prevBook.Chapters.Count && key > 0)
                  {
                    // ISSUE: variable of a compiler-generated type
                    BibleReader.\u003C\u003Ec__DisplayClass3 cDisplayClass3_1 = this;
                    // ISSUE: reference to a compiler-generated field
                    // ISSUE: reference to a compiler-generated field
                    cDisplayClass3_1.text = cDisplayClass3_1.text + " " + key.ToString();
                    if (match.Groups["vr"].Value.Length > 0)
                    {
                      num2 = DTU.atoi(match.Groups["vr"].Value);
                      if (num2 <= prevBook.Chapters[key].Verse.Count && num2 > 0)
                      {
                        // ISSUE: variable of a compiler-generated type
                        BibleReader.\u003C\u003Ec__DisplayClass3 cDisplayClass3_2 = this;
                        // ISSUE: reference to a compiler-generated field
                        // ISSUE: reference to a compiler-generated field
                        cDisplayClass3_2.text = cDisplayClass3_2.text + ":" + num2.ToString();
                      }
                      else
                      {
                        num2 = vr;
                        // ISSUE: variable of a compiler-generated type
                        BibleReader.\u003C\u003Ec__DisplayClass3 cDisplayClass3_2 = this;
                        // ISSUE: reference to a compiler-generated field
                        // ISSUE: reference to a compiler-generated field
                        cDisplayClass3_2.text = cDisplayClass3_2.text + ":" + num2.ToString();
                      }
                    }
                    else if (match.Value.Contains(":"))
                    {
                      // ISSUE: reference to a compiler-generated field
                      this.text += ":";
                    }
                  }
                  else
                  {
                    key = chp;
                    // ISSUE: variable of a compiler-generated type
                    BibleReader.\u003C\u003Ec__DisplayClass3 cDisplayClass3 = this;
                    // ISSUE: reference to a compiler-generated field
                    // ISSUE: reference to a compiler-generated field
                    cDisplayClass3.text = cDisplayClass3.text + " " + key.ToString();
                  }
                }
                else
                  text = name;
              }
            }
            else
              name = text = (string) null;
            if (name == null || text == null)
            {
              text = (string) null;
              return false;
            }
            if (bibleBook != null)
            {
              if (prevBook == null || prevBook.ID != bibleBook.ID)
              {
                prevBook = bibleBook;
                if (key > prevBook.Chapters.Count || key < 1)
                  key = chp;
                if (num2 > prevBook.Chapters[key].Verse.Count || num2 < 1)
                  num2 = vr;
                chp = key;
                vr = num2;
                if (chp > prevBook.Chapters.Count || chp < 1)
                  chp = 1;
                if (vr > prevBook.Chapters[key].Verse.Count || vr < 1)
                {
                  vr = 1;
                  break;
                }
                break;
              }
              if (key > prevBook.Chapters.Count || key < 1)
              {
                key = chp;
                if (key > prevBook.Chapters.Count || key < 1)
                  key = 1;
              }
              if (num2 > prevBook.Chapters[key].Verse.Count || num2 < 1)
              {
                num2 = vr;
                if (num2 > prevBook.Chapters[key].Verse.Count || num2 < 1)
                  num2 = 1;
              }
              if (chp != key || vr != num2)
              {
                chp = key;
                vr = num2;
              }
              break;
            }
            if (text.Length == 1 && (int) text[0] == (int) ch)
            {
              text = (string) null;
              break;
            }
          }
          return false;
        }));
        winConsole.Stop();
      }
    }

    public void CreateNamesText()
    {
      Encoding encoding = Encoding.GetEncoding(950);
      Regex regex1 = new Regex("\\<a href=.hb5\\/[^>]+\\>(?<ch>.*?)\\<\\/a\\>", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
      Regex regex2 = new Regex("^(?<ch>[^A-Z0-9]*?)(?<en>\\d? [A-Z][A-Z\\s]+)$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      using (StreamReader streamReader = new StreamReader("C:\\Users\\mchu\\Downloads\\hb5.htm", encoding))
      {
        string end = streamReader.ReadToEnd();
        StringBuilder stringBuilder = new StringBuilder();
        for (Match match1 = regex1.Match(end); match1.Success; match1 = match1.NextMatch())
        {
          string input = match1.Groups["ch"].Value;
          Match match2 = regex2.Match(input);
          string str = match2.Groups["en"].Value.Trim();
          BibleBook bibleBook = (BibleBook) null;
          if (this.BooksByName.TryGetValue(str.ToUpper(), out bibleBook))
            dictionary[bibleBook.ID] = match2.Groups["ch"].Value.Trim();
        }
      }
      using (StreamWriter streamWriter = new StreamWriter("C:\\Users\\mchu\\Downloads\\Names.csv", false, Encoding.Unicode))
      {
        foreach (BibleBook oldTestimentBook in this.OldTestimentBooks)
          streamWriter.WriteLine("{0},{1},{2}", (object) oldTestimentBook.ID, (object) oldTestimentBook.Name, (object) dictionary[oldTestimentBook.ID]);
        foreach (BibleBook newTestimentBook in this.NewTestimentBooks)
          streamWriter.WriteLine("{0},{1},{2}", (object) newTestimentBook.ID, (object) newTestimentBook.Name, (object) dictionary[newTestimentBook.ID]);
        streamWriter.Close();
      }
    }

    public BibleIndex Load(string path, bool create)
    {
      BibleIndex bibleIndex = new BibleIndex(path);
      if (!Directory.Exists(path))
      {
        if (!create)
          return (BibleIndex) null;
        bibleIndex.CreateIndex(this);
      }
      return bibleIndex;
    }
  }
}
