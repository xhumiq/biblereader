﻿﻿﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.Content.BibleBook
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using MCL;
using System.Collections.Generic;
using System.Xml;

namespace CMSMgmt.Content
{
  public class BibleBook
  {
    public readonly SortedDictionary<int, BibleChapter> Chapters = new SortedDictionary<int, BibleChapter>();

    public string ID { get; private set; }

    public string Name { get; internal set; }

    public void ReadOSISBook(XmlReader reader)
    {
      this.ID = reader.GetAttribute("osisID");
      this.Name = BibleReader.BookIDs[this.ID];
      while (reader.Read() && (reader.NodeType != XmlNodeType.EndElement || !(reader.LocalName == "div")))
      {
        if (reader.NodeType == XmlNodeType.Element)
        {
          if (reader.LocalName != "chapter")
            throw new SysException("Unknown xml element: {0}", new object[1]
            {
              (object) reader.LocalName
            });
          BibleChapter bibleChapter = new BibleChapter(this);
          bibleChapter.ReadOSISChapter(reader);
          this.Chapters.Add(bibleChapter.ChapNumber, bibleChapter);
        }
      }
    }
  }
}
