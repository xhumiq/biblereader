﻿﻿﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.Content.BibleChapter
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using MCL;
using System.Collections.Generic;
using System.Xml;

namespace CMSMgmt.Content
{
  public class BibleChapter
  {
    public readonly BibleBook Book;
    public readonly SortedDictionary<int, string> Verse = new SortedDictionary<int, string>();

    public int ChapNumber { get; private set; }

    internal BibleChapter(BibleBook book) => this.Book = book;

    public void ReadOSISChapter(XmlReader reader)
    {
      this.ChapNumber = DTU.atoi(reader.GetAttribute("osisID").Split('.')[1]);
      while (reader.Read() && (reader.NodeType != XmlNodeType.EndElement || !(reader.LocalName == "chapter")))
      {
        if (reader.NodeType == XmlNodeType.Element)
        {
          string[] strArray = !(reader.LocalName != "verse") ? reader.GetAttribute("osisID").Split('.') : throw new SysException("Unknown xml element: {0}", new object[1]
          {
            (object) reader.LocalName
          });
          while (reader.MoveToContent() != XmlNodeType.Text)
            reader.Read();
          this.Verse.Add(DTU.atoi(strArray[2]), reader.Value.Trim());
        }
      }
    }
  }
}
