﻿﻿﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.Content.BibleIndex
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Search.Vectorhighlight;
using Lucene.Net.Store;
using Lucene.Net.Util;
using MCL;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CMSMgmt.Content
{
  public class BibleIndex
  {
    public static readonly Regex REDocLoc = new Regex("^(?<id>[\\w]+)\\:(?<ch>\\d+)(?:\\.(?<vr>\\d+))?$", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);

    public string IndexPath { get; private set; }

    public BibleIndex(string path) => this.IndexPath = path;

    public void CreateIndex(BibleReader bible)
    {
      IndexWriter indexWriter = new IndexWriter((Lucene.Net.Store.Directory) FSDirectory.Open(new DirectoryInfo(this.IndexPath)), (Analyzer) new StandardAnalyzer(Version.LUCENE_29), true, IndexWriter.MaxFieldLength.LIMITED);
      StringBuilder stringBuilder = new StringBuilder();
      foreach (BibleBook bibleBook in bible.Books.Values)
      {
        for (int key1 = 1; key1 <= bibleBook.Chapters.Count; ++key1)
        {
          BibleChapter chapter = bibleBook.Chapters[key1];
          for (int key2 = 1; key2 <= chapter.Verse.Count; ++key2)
          {
            Document doc = new Document();
            string value_Renamed = bibleBook.ID + ":" + (object) key1 + "." + (object) key2;
            doc.Add((Fieldable) new Field("loc", value_Renamed, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add((Fieldable) new Field("verse", chapter.Verse[key2], Field.Store.YES, Field.Index.ANALYZED));
            indexWriter.AddDocument(doc);
            stringBuilder.Append(value_Renamed);
            stringBuilder.Append(" ");
            stringBuilder.Append(chapter.Verse[key2]);
            stringBuilder.Append("\r\n");
          }
          Document doc1 = new Document();
          doc1.Add((Fieldable) new Field("loc", bibleBook.ID + ":" + (object) key1, Field.Store.YES, Field.Index.NOT_ANALYZED));
          doc1.Add((Fieldable) new Field("book", bibleBook.ID, Field.Store.YES, Field.Index.NOT_ANALYZED));
          doc1.Add((Fieldable) new Field("ch", key1.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
          doc1.Add((Fieldable) new Field("chap", stringBuilder.ToString(), Field.Store.YES, Field.Index.ANALYZED));
          indexWriter.AddDocument(doc1);
          stringBuilder.Remove(0, stringBuilder.Length);
        }
      }
      indexWriter.Optimize();
      indexWriter.Close();
    }

    public BibleSearchMatch[] Search(string queryText)
    {
      FastVectorHighlighter vectorHighlighter = new FastVectorHighlighter(true, true);
      IndexReader r = IndexReader.Open((Lucene.Net.Store.Directory) FSDirectory.Open(new DirectoryInfo(this.IndexPath)), true);
      Searcher searcher = (Searcher) new IndexSearcher(r);
      Analyzer a = (Analyzer) new StandardAnalyzer(Version.LUCENE_29);
      Query query = new QueryParser(Version.LUCENE_29, "verse", a).Parse(queryText);
      TopScoreDocCollector scoreDocCollector = TopScoreDocCollector.create(20, false);
      searcher.Search(query, (Collector) scoreDocCollector);
      ScoreDoc[] scoreDocs = scoreDocCollector.TopDocs().scoreDocs;
      scoreDocCollector.GetTotalHits();
      vectorHighlighter.GetFieldQuery(query);
      List<BibleSearchMatch> bibleSearchMatchList = new List<BibleSearchMatch>();
      for (int index = 0; index < scoreDocs.Length; ++index)
      {
        ScoreDoc scoreDoc = scoreDocs[index];
        Document document = r.Document(scoreDoc.doc);
        string input = document.Get("loc");
        Match match = BibleIndex.REDocLoc.Match(input);
        if (!match.Success)
          throw new SysException("Loc format invalid: '{0}'", new object[1]
          {
            (object) input
          });
        BibleSearchMatch bibleSearchMatch = new BibleSearchMatch(match.Groups, document.Get("verse"));
        bibleSearchMatchList.Add(bibleSearchMatch);
      }
      return bibleSearchMatchList.ToArray();
    }
  }
}
