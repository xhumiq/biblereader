﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.App
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using MCL.SysMgmt;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace CMSMgmt
{
  public class App : Application
  {
    protected override void OnActivated(EventArgs e) => base.OnActivated(e);

    private void OnUnhandledEvent(object sender, DispatcherUnhandledExceptionEventArgs e) => TraceLogger.Send(e.Exception, "Unhandled Event");

    [DebuggerNonUserCode]
    public void InitializeComponent()
    {
      this.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(this.OnUnhandledEvent);
      this.StartupUri = new Uri("MainWin.xaml", UriKind.Relative);
    }

    [STAThread]
    [DebuggerNonUserCode]
    public static void Main()
    {
      App app = new App();
      app.InitializeComponent();
      app.Run();
    }
  }
}
