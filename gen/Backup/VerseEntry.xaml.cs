﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.VerseEntry
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using CMSMgmt.Content;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CMSMgmt
{
  public partial class VerseEntry : UserControl, IComponentConnector
  {
    internal Label cName;
    internal TextBlock cText;
    internal TextBlock cText2;
    private bool _contentLoaded;

    public VerseEntry() => this.InitializeComponent();

    public BibleBook Book { get; set; }

    public string Search { get; set; }

    public int Chapter { get; set; }

    public int Verse { get; set; }

    public string Source => this.cName.Content as string;

    public string EnglishText => this.cText.Text;

    public string ChineseText => this.cText2.Text;

    public void SetContent(string name, string english, string chinese)
    {
      this.cName.Content = (object) name;
      this.cText.Text = english;
      this.cText2.Text = chinese;
    }

    [DebuggerNonUserCode]
    public void InitializeComponent()
    {
      if (this._contentLoaded)
        return;
      this._contentLoaded = true;
      Application.LoadComponent((object) this, new Uri("/BibleReader;component/verseentry.xaml", UriKind.Relative));
    }

    [EditorBrowsable(EditorBrowsableState.Never)]
    [DebuggerNonUserCode]
    void IComponentConnector.Connect(int connectionId, object target)
    {
      switch (connectionId)
      {
        case 1:
          this.cName = (Label) target;
          break;
        case 2:
          this.cText = (TextBlock) target;
          break;
        case 3:
          this.cText2 = (TextBlock) target;
          break;
        default:
          this._contentLoaded = true;
          break;
      }
    }
  }
}
