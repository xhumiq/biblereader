﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.MainWin
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using CMSMgmt.Content;
using CMSMgmt.Data;
using MCL;
using MCL.SysMgmt;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace CMSMgmt
{
  public partial class MainWin : Window, IComponentConnector
  {
    private static Regex REGBText = new Regex("^(?:(?<pf>\\d)\\s{0,1})?(?:(?<name>[A-Z][A-Z ]*)\\s{0,1}(?:(?<ch>\\d+)(?:\\:(?<vr>\\d+)?)?)?)?$", RegexOptions.IgnoreCase);
    private BibleReader mBibleReader = new BibleReader();
    private BibleReader mBibleChinese = new BibleReader();
    private BibleIndex mBibleIndex = (BibleIndex) null;
    private BibleBook mBook = (BibleBook) null;
    private ListBoxItem mVerseEntry = (ListBoxItem) null;
    private VerseDisplay mDisplay = (VerseDisplay) null;
    private string mSearchString = (string) null;
    private string mBibleName = (string) null;
    private int mChapter = 0;
    private int mVerse = 0;
    private bool mInitDisplay = false;
    internal MainWin Main;
    internal Button cmdSearch;
    internal TextBlock cTitle;
    internal DockPanel pnlSearch;
    internal TextBox cSearch;
    internal ListBox cSearchResults;
    internal DockPanel pnlContents;
    internal TextBox cEntry;
    internal ListBox cNames;
    internal ListBox cBooks;
    internal ListBox cChapters;
    internal ListBox cVerses;
    private bool _contentLoaded;

    public MainWin()
    {
      this.InitializeComponent();
      this.mBibleReader.ReadOSISLang("eng");
      this.mBibleChinese.ReadOSISLang("chi");
      this.mBibleIndex = this.mBibleReader.Load(FileResStore.GetPath("Index", MCL.App.RootPath), true);
      this.mDisplay = new VerseDisplay();
      this.mDisplay.ShowActivated = false;
      this.mDisplay.Hide();
      PropertyStore.LoadMetrics((Window) this);
      foreach (BibleBook oldTestimentBook in this.mBibleChinese.OldTestimentBooks)
      {
        ListBoxItem listBoxItem = new ListBoxItem();
        listBoxItem.Content = (object) oldTestimentBook.Name;
        listBoxItem.Tag = (object) this.mBibleReader.Books[oldTestimentBook.ID];
        this.cNames.Items.Add((object) listBoxItem);
      }
      foreach (BibleBook newTestimentBook in this.mBibleChinese.NewTestimentBooks)
      {
        ListBoxItem listBoxItem = new ListBoxItem();
        listBoxItem.Content = (object) newTestimentBook.Name;
        listBoxItem.Tag = (object) this.mBibleReader.Books[newTestimentBook.ID];
        this.cNames.Items.Add((object) listBoxItem);
      }
    }

    private void OnLoaded(object sender, RoutedEventArgs e) => this.cEntry.Focus();

    private void OnNameKeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyboardDevice.IsKeyDown(Key.LeftCtrl) || e.KeyboardDevice.IsKeyDown(Key.RightCtrl) || (e.KeyboardDevice.IsKeyDown(Key.LeftAlt) || e.KeyboardDevice.IsKeyDown(Key.RightAlt)) || (e.KeyboardDevice.IsKeyDown(Key.LWin) || e.KeyboardDevice.IsKeyDown(Key.RWin)))
        return;
      if (e.Key == Key.Space && !this.UpdateChar(' '))
        e.Handled = true;
      if (e.Key != Key.Down)
        return;
      this.cVerses.Focus();
    }

    private void OnSearchKeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key != Key.Return)
        return;
      this.SearchBible(this.cSearch.Text.Trim());
    }

    private void SearchBible(string query)
    {
      this.cSearchResults.Items.Clear();
      BibleSearchMatch[] bibleSearchMatchArray = this.mBibleIndex.Search(query);
      if (bibleSearchMatchArray == null)
        return;
      foreach (BibleSearchMatch bibleSearchMatch in bibleSearchMatchArray)
      {
        VerseEntry entry = this.CreateEntry(this.mBibleReader.Books[bibleSearchMatch.ID], bibleSearchMatch.Chapter, bibleSearchMatch.Verse);
        ListBoxItem listBoxItem = new ListBoxItem();
        listBoxItem.Tag = (object) this.mBibleReader.Books[bibleSearchMatch.ID];
        listBoxItem.Content = (object) entry;
        this.cSearchResults.Items.Add((object) listBoxItem);
      }
    }

    private void OnTextInput(object sender, TextCompositionEventArgs e)
    {
      if (e.Text.Length != 1)
        TraceLogger.Send("!!! {0}", (object) e.Text);
      if (this.UpdateChar(e.Text[0]))
        return;
      e.Handled = true;
    }

    public bool UpdateChar(char ch)
    {
      BibleBook mBook = this.mBook;
      int mChapter = this.mChapter;
      int mVerse = this.mVerse;
      bool flag = this.ValidateChar(ch);
      if (this.mBook != null && (mBook == null || mBook.ID != this.mBook.ID || mChapter != this.mChapter || mVerse != this.mVerse))
      {
        this.UpdateBookEntries();
        this.UpdateChpEntries();
        if (mBook == null || mBook.ID != this.mBook.ID || mChapter != this.mChapter)
        {
          this.UpdateVersesEntries();
          if (this.mVerseEntry != null)
            this.ShowEntry((VerseEntry) this.mVerseEntry.Content);
        }
        else if (mVerse <= this.cVerses.Items.Count)
        {
          this.mVerseEntry = (ListBoxItem) this.cVerses.Items[mVerse];
          this.mVerseEntry.BringIntoView();
          this.ShowEntry((VerseEntry) this.mVerseEntry.Content);
        }
      }
      return flag;
    }

    public bool ValidateChar(char ch)
    {
      try
      {
        string text = this.cEntry.Text;
        string composition = this.cEntry.SelectionStart < text.Length ? text.Insert(this.cEntry.SelectionStart, ch.ToString()) : text + (object) ch;
        return this.ValidateChar(ch, composition);
      }
      catch (System.Exception ex)
      {
        TraceLogger.Send(ex, "Validate Char '{0}'", (object) ch);
        return false;
      }
    }

    public bool ValidateChar(char ch, string composition)
    {
      this.mSearchString = composition;
      Match match = MainWin.REGBText.Match(composition);
      bool flag = false;
      if (!match.Success)
      {
        if (ch != ':' && ch != '.' || this.cEntry.Text.Length <= 0 || this.cEntry.Text.Contains(":"))
          return false;
        composition = this.cEntry.Text;
        flag = true;
      }
      string mBibleName = this.mBibleName;
      string name = "";
      int key = 0;
      int num1 = 0;
      if (match.Groups["pf"].Value.Length > 0)
        name = match.Groups["pf"].Value + " ";
      string str1;
      if ((str1 = match.Groups["name"].Value.Trim()).Length > 0)
        str1 = (name += str1);
      if (match.Groups[nameof (ch)].Value.Length > 0)
        key = DTU.atoi(match.Groups[nameof (ch)].Value);
      if (match.Groups["vr"].Value.Length > 0)
        num1 = DTU.atoi(match.Groups["vr"].Value);
      BibleBook nearest;
      this.mBook = nearest = this.mBibleReader.FindNearest(name);
      if (nearest == null)
        return false;
      if (!nearest.Name.ToUpper().StartsWith(str1.ToUpper()))
      {
        int length = str1.Length;
        if (nearest.Name.Length < length)
          length = nearest.Name.Length;
        string str2 = this.mBibleName = nearest.Name.Substring(0, length);
        if (key > 0)
          this.mChapter = key;
        if (num1 > 0)
          this.mVerse = num1;
        if (num1 > 0 || key > 0)
        {
          str2 = str2 + " " + this.mChapter.ToString();
          if (num1 > 0)
            str2 = str2 + ":" + this.mVerse.ToString();
        }
        else if (flag)
          str2 = str2 + " " + this.mChapter.ToString() + ":";
        int num2 = this.cEntry.SelectionStart + 1;
        if (num2 >= this.cEntry.Text.Length)
          num2 = 1000;
        this.cEntry.Text = str2;
        if (num2 > str2.Length)
          num2 = str2.Length;
        this.cEntry.SelectionStart = num2;
        return false;
      }
      if (match.Groups["name"].Value.Contains("  "))
        return false;
      if (char.IsLetter(ch))
        return true;
      if (flag)
      {
        if (num1 > 0)
          return false;
        string str2 = name + " " + this.mChapter.ToString() + ":";
        this.cEntry.Text = str2;
        this.cEntry.SelectionStart = str2.Length;
        return false;
      }
      if (match.Groups[nameof (ch)].Success && match.Groups[nameof (ch)].Index + match.Groups[nameof (ch)].Length > this.cEntry.SelectionStart)
      {
        if (key < 2)
          return true;
        if (key > nearest.Chapters.Count)
          return false;
        this.mChapter = key;
      }
      if (match.Groups["vr"].Success && match.Groups["vr"].Index + match.Groups["vr"].Length > this.cEntry.SelectionStart && num1 >= 2)
      {
        if (key < 1 || key > nearest.Chapters.Count)
          key = this.mChapter;
        else
          this.mChapter = key;
        if (key < 1)
          key = 1;
        else if (key > nearest.Chapters.Count)
          key = nearest.Chapters.Count;
        if (num1 > nearest.Chapters[key].Verse.Count)
          return false;
        this.mVerse = num1;
      }
      return true;
    }

    public void UpdateBookEntries()
    {
      string mSearchString = this.mSearchString;
      if (mSearchString == null || mSearchString.Length < 1)
        return;
      Match match = MainWin.REGBText.Match(mSearchString);
      if (!match.Success)
        return;
      BibleBook[] books = this.mBibleReader.FindBooks(match.Groups["name"].Value);
      if (books.Length < 1)
        return;
      this.cBooks.Items.Clear();
      foreach (BibleBook book in books)
      {
        int num = this.mChapter;
        int verse = this.mVerse;
        if (num < 1)
          num = 1;
        else if (num > book.Chapters.Count)
          num = book.Chapters.Count;
        if (verse < 1)
          verse = 1;
        else if (verse > book.Chapters[num].Verse.Count)
          verse = book.Chapters[num].Verse.Count;
        ListBoxItem listBoxItem = new ListBoxItem();
        listBoxItem.BorderThickness = new Thickness(0.0, 1.0, 0.0, 0.0);
        listBoxItem.BorderBrush = (Brush) Brushes.Gray;
        VerseEntry entry = this.CreateEntry(book, num, verse);
        entry.Book = book;
        entry.Chapter = num;
        entry.Verse = verse;
        listBoxItem.Content = (object) entry;
        this.cBooks.Items.Add((object) listBoxItem);
        if (this.mBook != null && this.mBook.ID == book.ID)
          listBoxItem.BringIntoView();
      }
    }

    public void UpdateVersesEntries()
    {
      this.mVerseEntry = (ListBoxItem) null;
      if (this.mBook == null)
        return;
      this.cVerses.Items.Clear();
      int num1 = this.mChapter;
      if (num1 < 1)
        num1 = 1;
      else if (num1 > this.mBook.Chapters.Count)
        num1 = this.mBook.Chapters.Count;
      int num2 = this.mVerse;
      if (num2 < 1)
        num2 = 1;
      else if (num2 > this.mBook.Chapters[num1].Verse.Count)
        num2 = this.mBook.Chapters[num1].Verse.Count;
      for (int verse = 1; verse <= this.mBook.Chapters[num1].Verse.Count; ++verse)
      {
        ListBoxItem listBoxItem = new ListBoxItem();
        listBoxItem.BorderThickness = new Thickness(0.0, 1.0, 0.0, 0.0);
        listBoxItem.BorderBrush = (Brush) Brushes.Gray;
        ((VerseEntry) (listBoxItem.Content = (object) this.CreateEntry(this.mBook, num1, verse))).Book = this.mBook;
        this.cVerses.Items.Add((object) listBoxItem);
        if (verse == num2)
          (this.mVerseEntry = listBoxItem).BringIntoView();
      }
    }

    public void UpdateChpEntries()
    {
      if (this.mBook == null)
        return;
      this.cChapters.Items.Clear();
      int num = this.mChapter;
      if (num < 1)
        num = 1;
      else if (num > this.mBook.Chapters.Count)
        num = this.mBook.Chapters.Count;
      for (int index = 1; index <= this.mBook.Chapters.Count; ++index)
      {
        int verse = this.mVerse;
        if (verse < 1)
          verse = 1;
        else if (verse > this.mBook.Chapters[index].Verse.Count)
          verse = this.mBook.Chapters[index].Verse.Count;
        ListBoxItem listBoxItem = new ListBoxItem();
        listBoxItem.BorderThickness = new Thickness(0.0, 1.0, 0.0, 0.0);
        listBoxItem.BorderBrush = (Brush) Brushes.Gray;
        VerseEntry entry;
        listBoxItem.Content = (object) (entry = this.CreateEntry(this.mBook, index, verse));
        entry.Book = this.mBook;
        entry.Chapter = index;
        entry.Verse = verse;
        this.cChapters.Items.Add((object) listBoxItem);
        if (index == num)
          listBoxItem.BringIntoView();
      }
    }

    public VerseEntry CreateEntry(BibleBook book, int chp, int verse)
    {
      VerseEntry verseEntry = new VerseEntry();
      string name = string.Format("{0} {1} {2}:{3}", (object) this.mBibleChinese.Books[book.ID].Name, (object) book.Name, (object) chp, (object) verse);
      verseEntry.Search = string.Format("{0} {1}:{2}", (object) book.Name, (object) chp, (object) verse);
      verseEntry.SetContent(name, book.Chapters[chp].Verse[verse], this.mBibleChinese.Books[book.ID].Chapters[chp].Verse[verse]);
      verseEntry.Chapter = chp;
      verseEntry.Verse = verse;
      return verseEntry;
    }

    private void OnSearchSelected(object sender, SelectionChangedEventArgs e)
    {
    }

    private void OnCNameSelected(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count <= 0)
        return;
      try
      {
        this.mBook = ((FrameworkElement) e.AddedItems[0]).Tag as BibleBook;
        this.UpdateChpEntries();
        this.UpdateVersesEntries();
      }
      catch (System.Exception ex)
      {
        TraceLogger.Send(ex, "CName Selected");
      }
    }

    private void OnBookSelected(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count <= 0)
        return;
      VerseEntry content = ((ContentControl) e.AddedItems[0]).Content as VerseEntry;
      this.mBook = content.Book;
      this.mChapter = content.Chapter;
      this.mVerse = content.Verse;
      this.cEntry.Text = this.mSearchString = content.Search;
      this.ShowEntry(content);
      this.UpdateChpEntries();
      this.UpdateVersesEntries();
    }

    private void OnChapterSelected(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count <= 0)
        return;
      VerseEntry content = ((ContentControl) e.AddedItems[0]).Content as VerseEntry;
      this.mBook = content.Book;
      this.mChapter = content.Chapter;
      this.mVerse = content.Verse;
      this.cEntry.Text = this.mSearchString = content.Search;
      this.ShowEntry(content);
      this.UpdateVersesEntries();
    }

    private void OnVerseSelected(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count <= 0)
        return;
      VerseEntry content = ((ContentControl) e.AddedItems[0]).Content as VerseEntry;
      this.cEntry.Text = content.Search;
      this.ShowEntry(content);
    }

    private void ShowEntry(VerseEntry ve) => this.mDisplay.SetContent(ve);

    private void ShowDisplay()
    {
      try
      {
        if (!this.mDisplay.IsVisible)
        {
          if (!this.mInitDisplay)
          {
            PropertyStore.LoadMetrics((Window) this.mDisplay);
            this.mInitDisplay = true;
            if (this.mDisplay.WindowState == WindowState.Maximized)
            {
              this.mDisplay.mMaximize = true;
              this.mDisplay.WindowState = WindowState.Normal;
            }
            this.mDisplay.ShowActivated = true;
            this.mDisplay.Show();
            if (!this.mDisplay.mMaximize)
              return;
            this.mDisplay.WindowState = WindowState.Maximized;
          }
          else
          {
            this.mDisplay.ShowActivated = true;
            this.mDisplay.Show();
          }
        }
        else
        {
          if (this.mDisplay.WindowState != WindowState.Minimized)
            return;
          if (this.mDisplay.mMaximize)
            this.mDisplay.WindowState = WindowState.Maximized;
          this.mDisplay.SelfAdjust();
        }
      }
      catch (System.Exception ex)
      {
        TraceLogger.Send(ex, "Unable to activate");
      }
    }

    private void OnUnloaded(object sender, RoutedEventArgs e)
    {
      if (this.mDisplay.IsVisible)
        PropertyStore.StoreMetrics((Window) this.mDisplay);
      this.mDisplay.CloseWindow = true;
      this.mDisplay.Close();
      PropertyStore.StoreMetrics((Window) this);
    }

    private void OnIncFontClick(object sender, RoutedEventArgs e) => this.mDisplay.IncFontSize();

    private void OnAdjFontClick(object sender, RoutedEventArgs e) => this.mDisplay.SelfAdjust(true);

    private void OnCloseDispClick(object sender, RoutedEventArgs e)
    {
      if (!this.mDisplay.IsVisible)
        return;
      this.mDisplay.CloseHide();
    }

    private void OnShowDispClick(object sender, RoutedEventArgs e) => this.ShowDisplay();

    private void OnSearchDispClick(object sender, RoutedEventArgs e)
    {
      if (this.cmdSearch.Content.ToString() == "Search")
      {
        this.pnlContents.Visibility = Visibility.Collapsed;
        this.pnlSearch.Visibility = Visibility.Visible;
        this.cSearch.Focus();
        this.cmdSearch.Content = (object) "Bible";
        this.cTitle.Text = "Enter Search Term";
      }
      else
      {
        this.pnlContents.Visibility = Visibility.Visible;
        this.pnlSearch.Visibility = Visibility.Collapsed;
        this.cEntry.Focus();
        this.cmdSearch.Content = (object) "Search";
        this.cTitle.Text = "Enter Bible Location";
      }
    }

    private void OnDecFontClick(object sender, RoutedEventArgs e) => this.mDisplay.DecFontSize();

    private void OnVersesKeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.D)
        this.cEntry.Focus();
      if (this.mBook == null)
        return;
      if (e.Key == Key.Left)
      {
        if (this.mChapter <= 1)
          return;
        --this.mChapter;
        this.UpdateVersesEntries();
        if (this.mVerseEntry != null)
        {
          this.mVerseEntry.Focus();
          this.cVerses.SelectedItem = (object) this.mVerseEntry;
        }
      }
      else
      {
        if (e.Key != Key.Right || this.mChapter >= this.mBook.Chapters.Count)
          return;
        ++this.mChapter;
        this.UpdateVersesEntries();
        if (this.mVerseEntry != null)
        {
          this.mVerseEntry.Focus();
          this.cVerses.SelectedItem = (object) this.mVerseEntry;
        }
      }
    }

    private void OnWindowKeyDown(object sender, KeyEventArgs e)
    {
    }

    private void OnFocus(object sender, RoutedEventArgs e) => this.cEntry.SelectAll();

    private void OnGridLoaded(object sender, RoutedEventArgs e)
    {
    }

    private void OnVerseDblClick(object sender, MouseButtonEventArgs e)
    {
      try
      {
        if (this.mDisplay.IsVisible)
          return;
        this.ShowDisplay();
      }
      catch (System.Exception ex)
      {
        TraceLogger.Send(ex, "CName Selected");
      }
    }

    private void OnSearchDblClick(object sender, MouseButtonEventArgs e)
    {
      try
      {
        ListBoxItem selectedItem = this.cSearchResults.SelectedItem as ListBoxItem;
        this.mBook = selectedItem.Tag as BibleBook;
        VerseEntry content = selectedItem.Content as VerseEntry;
        this.mChapter = content.Chapter;
        this.mVerse = content.Verse;
        this.pnlContents.Visibility = Visibility.Visible;
        this.pnlSearch.Visibility = Visibility.Collapsed;
        this.cEntry.Text = string.Format("{0} {1}:{2}", (object) this.mBook.Name, (object) this.mChapter, (object) this.mVerse);
        this.cEntry.Focus();
        this.cmdSearch.Content = (object) "Search";
        this.cTitle.Text = "Enter Bible Location";
        this.UpdateChpEntries();
        this.UpdateVersesEntries();
        this.ShowEntry(content);
      }
      catch (System.Exception ex)
      {
        TraceLogger.Send(ex, "CName Selected");
      }
    }

    [DebuggerNonUserCode]
    public void InitializeComponent()
    {
      if (this._contentLoaded)
        return;
      this._contentLoaded = true;
      Application.LoadComponent((object) this, new Uri("/BibleReader;component/mainwin.xaml", UriKind.Relative));
    }

    [EditorBrowsable(EditorBrowsableState.Never)]
    [DebuggerNonUserCode]
    void IComponentConnector.Connect(int connectionId, object target)
    {
      switch (connectionId)
      {
        case 1:
          this.Main = (MainWin) target;
          this.Main.Loaded += new RoutedEventHandler(this.OnLoaded);
          this.Main.Unloaded += new RoutedEventHandler(this.OnUnloaded);
          this.Main.PreviewKeyDown += new KeyEventHandler(this.OnWindowKeyDown);
          break;
        case 2:
          ((ButtonBase) target).Click += new RoutedEventHandler(this.OnIncFontClick);
          break;
        case 3:
          ((ButtonBase) target).Click += new RoutedEventHandler(this.OnDecFontClick);
          break;
        case 4:
          ((ButtonBase) target).Click += new RoutedEventHandler(this.OnAdjFontClick);
          break;
        case 5:
          ((ButtonBase) target).Click += new RoutedEventHandler(this.OnCloseDispClick);
          break;
        case 6:
          ((ButtonBase) target).Click += new RoutedEventHandler(this.OnShowDispClick);
          break;
        case 7:
          this.cmdSearch = (Button) target;
          this.cmdSearch.Click += new RoutedEventHandler(this.OnSearchDispClick);
          break;
        case 8:
          this.cTitle = (TextBlock) target;
          break;
        case 9:
          this.pnlSearch = (DockPanel) target;
          break;
        case 10:
          this.cSearch = (TextBox) target;
          this.cSearch.PreviewKeyDown += new KeyEventHandler(this.OnSearchKeyDown);
          this.cSearch.GotFocus += new RoutedEventHandler(this.OnFocus);
          break;
        case 11:
          this.cSearchResults = (ListBox) target;
          this.cSearchResults.SelectionChanged += new SelectionChangedEventHandler(this.OnSearchSelected);
          this.cSearchResults.MouseDoubleClick += new MouseButtonEventHandler(this.OnSearchDblClick);
          break;
        case 12:
          this.pnlContents = (DockPanel) target;
          break;
        case 13:
          this.cEntry = (TextBox) target;
          this.cEntry.PreviewKeyDown += new KeyEventHandler(this.OnNameKeyDown);
          this.cEntry.PreviewTextInput += new TextCompositionEventHandler(this.OnTextInput);
          this.cEntry.GotFocus += new RoutedEventHandler(this.OnFocus);
          break;
        case 14:
          ((UIElement) target).KeyDown += new KeyEventHandler(this.OnVersesKeyDown);
          ((FrameworkElement) target).Loaded += new RoutedEventHandler(this.OnGridLoaded);
          break;
        case 15:
          this.cNames = (ListBox) target;
          this.cNames.SelectionChanged += new SelectionChangedEventHandler(this.OnCNameSelected);
          break;
        case 16:
          this.cBooks = (ListBox) target;
          this.cBooks.SelectionChanged += new SelectionChangedEventHandler(this.OnBookSelected);
          break;
        case 17:
          this.cChapters = (ListBox) target;
          this.cChapters.SelectionChanged += new SelectionChangedEventHandler(this.OnChapterSelected);
          break;
        case 18:
          this.cVerses = (ListBox) target;
          this.cVerses.SelectionChanged += new SelectionChangedEventHandler(this.OnVerseSelected);
          this.cVerses.MouseDoubleClick += new MouseButtonEventHandler(this.OnVerseDblClick);
          break;
        default:
          this._contentLoaded = true;
          break;
      }
    }
  }
}
