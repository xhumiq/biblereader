﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.Properties.Resources
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace CMSMgmt.Properties
{
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (CMSMgmt.Properties.Resources.resourceMan == null)
          CMSMgmt.Properties.Resources.resourceMan = new ResourceManager("CMSMgmt.Properties.Resources", typeof (CMSMgmt.Properties.Resources).Assembly);
        return CMSMgmt.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get => CMSMgmt.Properties.Resources.resourceCulture;
      set => CMSMgmt.Properties.Resources.resourceCulture = value;
    }
  }
}
