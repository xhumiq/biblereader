﻿// Decompiled with JetBrains decompiler
// Type: CMSMgmt.VerseDisplay
// Assembly: BibleReader, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DF658378-B201-49F5-9C48-F6EE532FAFFA
// Assembly location: C:\ntc\builds\BibleReader\oldBin\BibleReader.exe

using CMSMgmt.Data;
using MCL.SysMgmt;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Threading;

namespace CMSMgmt
{
  public partial class VerseDisplay : Window, IComponentConnector
  {
    private double mCoef = 0.0;
    internal bool CloseWindow = false;
    internal bool mMaximize = false;
    internal bool mIgnoreSize = false;
    private static Action EmptyDelegate = (Action) (() => {});
    internal VerseDisplay Display;
    internal StackPanel cBox;
    internal Label cName;
    internal TextBlock cText2;
    internal TextBlock cText;
    private bool _contentLoaded;

    public VerseDisplay() => this.InitializeComponent();

    public string Source => this.cName.Content as string;

    public string EnglishText => this.cText.Text;

    public string ChineseText => this.cText2.Text;

    public void SetContent(VerseEntry entry) => this.SetContent(entry.Source, entry.EnglishText, entry.ChineseText);

    public void SetContent(string name, string english, string chinese)
    {
      this.cName.Content = (object) name;
      this.cText.Text = english;
      this.cText2.Text = chinese;
      this.RefreshContent();
      if (!this.IsVisible)
        return;
      this.SelfAdjust();
    }

    private double ContentHeight
    {
      get
      {
        Size renderSize = this.cText.RenderSize;
        double height1 = renderSize.Height;
        renderSize = this.cText2.RenderSize;
        double height2 = renderSize.Height;
        double num = height1 + height2;
        renderSize = this.cName.RenderSize;
        double height3 = renderSize.Height;
        return num + height3;
      }
    }

    private void OnSizeChanged(object sender, SizeChangedEventArgs e) => this.SelfAdjust(e.NewSize.Height - 50.0);

    private void SetCoef(double coef, double height)
    {
      this.mCoef = coef;
      if (0.3 * coef * height < 7.0)
      {
        TraceLogger.Send("!!! size is too small: {0:0.0} {1:0.0} {2:0.0}", (object) this.cText.FontSize, (object) height, (object) this.ContentHeight);
      }
      else
      {
        this.cName.FontSize = 0.3 * coef * height;
        this.cText.FontSize = 0.35 * coef * height;
        this.cText2.FontSize = 0.35 * coef * height;
        this.cName.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
        this.cText.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
        this.cText2.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      }
    }

    private void SetCoefTitle(double coef, double height)
    {
      if (0.3 * coef * height < 7.0)
      {
        TraceLogger.Send("!!! size is too small: {0:0.0} {1:0.0} {2:0.0}", (object) this.cText.FontSize, (object) height, (object) this.ContentHeight);
      }
      else
      {
        this.cName.FontSize = 0.3 * coef * height;
        this.cName.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      }
    }

    public void SelfAdjust() => this.SelfAdjust(this.RenderSize.Height - 50.0);

    public void SelfAdjust(bool force) => this.SelfAdjust(this.RenderSize.Height - 50.0, force);

    public void SelfAdjust(double height) => this.SelfAdjust(height, false);

    public void SelfAdjust(double height, bool force)
    {
      if (this.mIgnoreSize)
      {
        this.mIgnoreSize = false;
      }
      else
      {
        if (!this.IsLoaded)
          return;
        try
        {
          if (this.mCoef == 0.0 || Math.Abs(height - this.ContentHeight) > height * 0.45)
          {
            this.SetCoef(0.2, height);
            force = true;
          }
          else
            this.SetCoefTitle(0.2, height);
          int num = 0;
          if (force)
          {
            for (int index = 0; index < 50; ++index)
            {
              if (num == 0)
                num = height < this.ContentHeight ? -1 : 1;
              double fontSize = this.cText.FontSize;
              this.SelfAdjustHeight(height);
              if (this.cText.FontSize == fontSize || (height < this.ContentHeight ? -1 : 1) != num)
                break;
            }
          }
          for (int index = 0; index < 50 && height <= this.ContentHeight; ++index)
            this.SelfAdjustHeight(height);
        }
        catch (Exception ex)
        {
          TraceLogger.Send(ex, "Size: {0} {1}", (object) height, (object) this.cBox.RenderSize.Height);
        }
      }
    }

    private void SelfAdjustHeight(double height)
    {
      if (height < this.ContentHeight)
      {
        this.SetSize(this.cText.FontSize - 1.0);
      }
      else
      {
        if (this.ContentHeight + 10.0 >= height)
          return;
        this.SetSize(this.cText.FontSize + 1.0);
      }
    }

    public void IncFontSize() => this.SetSize(this.cText.FontSize + 0.5);

    public void DecFontSize() => this.SetSize(this.cText.FontSize - 0.5);

    private void SetSize(double fontSize)
    {
      this.cText.FontSize = fontSize;
      this.cText2.FontSize = fontSize;
      this.RefreshContent();
    }

    private void RefreshContent()
    {
      this.cName.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      this.cText.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      this.cText2.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
    }

    public void CloseHide()
    {
      PropertyStore.StoreMetrics((Window) this);
      this.Visibility = Visibility.Collapsed;
      this.Hide();
    }

    private void OnClosing(object sender, CancelEventArgs e)
    {
      if (this.CloseWindow)
        return;
      e.Cancel = true;
      this.CloseHide();
    }

    [DebuggerNonUserCode]
    public void InitializeComponent()
    {
      if (this._contentLoaded)
        return;
      this._contentLoaded = true;
      Application.LoadComponent((object) this, new Uri("/BibleReader;component/versedisplay.xaml", UriKind.Relative));
    }

    [EditorBrowsable(EditorBrowsableState.Never)]
    [DebuggerNonUserCode]
    void IComponentConnector.Connect(int connectionId, object target)
    {
      switch (connectionId)
      {
        case 1:
          this.Display = (VerseDisplay) target;
          this.Display.SizeChanged += new SizeChangedEventHandler(this.OnSizeChanged);
          this.Display.Closing += new CancelEventHandler(this.OnClosing);
          break;
        case 2:
          this.cBox = (StackPanel) target;
          break;
        case 3:
          this.cName = (Label) target;
          break;
        case 4:
          this.cText2 = (TextBlock) target;
          break;
        case 5:
          this.cText = (TextBlock) target;
          break;
        default:
          this._contentLoaded = true;
          break;
      }
    }
  }
}
