﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using BibleStore;
using MCL.Logging;

namespace BibleLive {
  /// <summary>
  /// Interaction logic fOnClosingzeChangedml
  /// </summary>
  public partial class VerseDisplay : Window {
    public VerseDisplay() {
      InitializeComponent();
    }

    private void OnSizeChanged(object sender, SizeChangedEventArgs e) => this.SelfAdjust(e.NewSize.Height - 50.0);

    private void OnClosing(object sender, CancelEventArgs e) {
      if (this.CloseWindow)
        return;
      e.Cancel = true;
      this.CloseHide();
    }
    
    public void SetContent(BibleSearchMatch entry){
      this.CName.Content = string.Format($"{entry.BookNames["zh"]}  {entry.BookNames["en"]} {entry.Chapter.ToString()}:{entry.SelectedVerse.Verse.ToString()}");
      this.CText.Text = entry.SelectedVerse.Content["en"];
      this.CText2.Text = entry.SelectedVerse.Content["zh"];
      this.RefreshContent();
      if (!this.IsVisible)return;
      this.SelfAdjust();
      TraceLogger.Send("Set Content Self Adjust");
    }

    private void SetCoef(double coef, double height){
      this.mCoef = coef;
      if (0.3 * coef * height < 7.0) {
        TraceLogger.Send("!!! size is too small: {0:0.0} {1:0.0} {2:0.0}", this.CText.FontSize.ToString(), height.ToString(), this.ContentHeight.ToString());
      } else {
        this.CName.FontSize = 0.3 * coef * height;
        this.CText.FontSize = 0.35 * coef * height;
        this.CText2.FontSize = 0.35 * coef * height;
        this.CName.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
        this.CText.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
        this.CText2.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      }
    }

    private void SetCoefTitle(double coef, double height) {
      if (0.3 * coef * height < 7.0) {
        TraceLogger.Send("!!! size is too small: {0:0.0} {1:0.0} {2:0.0}", this.CText.FontSize.ToString(), height.ToString(), this.ContentHeight.ToString());
      } else {
        this.CName.FontSize = 0.3 * coef * height;
        this.CName.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      }
    }
    
    private void RefreshContent() {
      this.CName.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      this.CText.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
      this.CText2.Dispatcher.Invoke(DispatcherPriority.Render, (Delegate) VerseDisplay.EmptyDelegate);
    }

    public void IncFontSize() => this.SetSize(this.CText.FontSize + 0.5);

    public void DecFontSize() => this.SetSize(this.CText.FontSize - 0.5);
    public void SelfAdjust() => this.SelfAdjust(this.RenderSize.Height - 50.0);
    public bool SelfAdjust(double height, bool force) {
      if (this.mIgnoreSize) {
        this.mIgnoreSize = false;
      }else {
        if (!this.IsLoaded) return false;
        this.CText.Visibility = Visibility.Hidden;
        this.CText2.Visibility = Visibility.Hidden;
        try {
          if (this.mCoef == 0.0 || Math.Abs(height - this.ContentHeight) > height * 0.45) {
            this.SetCoef(0.2, height);
            force = true;
          }else this.SetCoefTitle(0.2, height);
          int num = 0;
          var oh = this.ContentHeight;
          if (force) {
            for (int index = 0; index < 50; ++index) {
              if (num == 0)num = height < this.ContentHeight ? -1 : 1;
              double fontSize = this.CText.FontSize;
              if (!this.SelfAdjustHeight(oh, height)) break;
              if (this.CText.FontSize == fontSize || (height < this.ContentHeight ? -1 : 1) != num) break;
            }
          }

          for (int index = 0; index < 50 && height <= this.ContentHeight; ++index) {
            if (!this.SelfAdjustHeight(oh, height)) break;
          }
        } catch (Exception ex) {
          TraceLogger.Send(ex, "Size: {0} {1}", height.ToString(), this.CBox.RenderSize.Height.ToString());
        }
        this.CText.Visibility = Visibility.Visible;
        this.CText2.Visibility = Visibility.Visible;
      }
      return false;
    }

    private void SetSize(double fontSize) {
      this.CText.FontSize = fontSize;
      this.CText2.FontSize = fontSize;
      this.RefreshContent();
    }

    public bool SelfAdjust(bool force) => this.SelfAdjust(this.RenderSize.Height - 50.0, force);

    public bool SelfAdjust(double height) => this.SelfAdjust(height, false);
    private bool SelfAdjustHeight(double orgHeight, double height) {
      var ch = this.ContentHeight;
      if (height < ch) {
        this.SetSize(this.CText.FontSize - 1.0);
        return true;
      } else {
        if (((height - ch) / ch) < 0.8) {
          if (ch + 10.0 >= height) return false;
          if (((ch - orgHeight) / ch) > 0.10) return false;
        }
        this.SetSize(this.CText.FontSize + 1.0);
        return true;
      }
      return false;
    }

    private double ContentHeight {
      get {
        Size renderSize = this.CText.RenderSize;
        double height1 = renderSize.Height;
        renderSize = this.CText2.RenderSize;
        double height2 = renderSize.Height;
        double num = height1 + height2;
        renderSize = this.CName.RenderSize;
        double height3 = renderSize.Height;
        return num + height3;
      }
    }

    private void OnDisplayKeyDown(object sender, KeyEventArgs e) {
      if (e.Key == Key.Escape) {
        this.Close();
      }
    }

    public void CloseHide() {
      //PropertyStore.StoreMetrics((Window) this);
      this.Visibility = Visibility.Collapsed;
      this.Hide();
      PropertyStore.StoreMetrics((Window) this);
    }
    private double mCoef = 0.0;
    internal bool CloseWindow = false;
    internal bool mIgnoreSize = false;
    internal bool mMaximize = false;
    private SolidColorBrush backGroundBrush = new SolidColorBrush(Colors.White);
    private static Action EmptyDelegate = (Action) (() => {});
  }
}
