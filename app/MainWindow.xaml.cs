﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using BibleStore;
using MCL;
using MCL.Logging;

namespace BibleLive {
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window {
    private VerseDisplay mDisplay = (VerseDisplay) null;
    public MainWindow() {
      InitializeComponent();
      this.mDisplay = new VerseDisplay();
      this.mDisplay.ShowActivated = false;
      this.mDisplay.Hide();
      PropertyStore.LoadMetrics((Window) this);
      LoadNames(BibleStore.BibleStore.ChiBible);
    }

    private void OnContentRendered(object sender, EventArgs e) {
      this.CEntry.Focus(); 
    }
    
    private void OnTextChanged(object sender, TextChangedEventArgs e) {
      if (this.refreshing) return;
      try {
        this.refreshing = true;
        FindEn(this.CEntry.Text);
        return;
      } catch (Exception E) {
        TraceLogger.Send(E, "On Text Input");
      } finally {
        this.refreshing = false;
      }
      //e.Handled = true;
    }

    private void OnCNameSelected(object sender, SelectionChangedEventArgs e) {
      if (this.refreshing) return;
      if (e.AddedItems.Count <= 0)return;
      var bk = ((FrameworkElement) e.AddedItems[0]).Tag as BibleBook;
      if (bk == null) return;
      BibleName bkn = null;
      if (BibleStore.BibleStore.EngBookNames.TryGetValue(bk.Id.ToLower(), out bkn)) {
        this.refreshing = true;
        this.CEntry.Text = bkn.Full;
        try {
          FindEn(bkn.Full);
        } catch (Exception E) {
          TraceLogger.Send(E, "On Text Input");
        }
        this.refreshing = false;
      }
      //this.CEntry.Focus();
    }
    
    public bool FindEn(string composition) {
      searchReesults = BibleStore.BibleStore.FindEn(composition);
      return RefreshBooks(searchReesults);
    }

    public ListBoxItem CreateItem(BibleSearchMatch match, BibleSearchVerse verse) {
      return new ListBoxItem() {
        BorderThickness = new Thickness(0.0, 1.0, 0.0, 0.0),
        BorderBrush = (Brush) Brushes.Gray,
        Tag = match.Clone(verse), Content = CreateEntry(match, verse)
      };
    }

    public bool RefreshBooks(BibleSearchResults results) {
      this.CBooks.Items.Clear();
      if (results == null || results.Matches == null || results.Matches.Length < 1) return false;
      foreach (var item in results.Matches.Select(x => CreateItem(x, x.Verses.First()))) {
        this.CBooks.Items.Add(item);
      }
      if (this.CBooks.Items.Count > 0) {
        this.CBooks.SelectedItem = this.CBooks.Items[0];
      }
      return true;
    }

    private void OnSearchKeyDown(object sender, KeyEventArgs e) {
      if (e.Key != Key.Return) return;
      SearchBible(this.CEntry.Text.Trim());
    }

    private void OnBookSelected(object sender, SelectionChangedEventArgs e) {
      if (this.refreshChapt) return;
      this.refreshChapt = true;
      this.refreshVerses = true;
      this.CChapters.Items.Clear();
      this.CVerses.Items.Clear();
      if (e.AddedItems.Count > 0) {
        var item = e.AddedItems[0] as ListBoxItem;
        var sm = item.Tag as BibleSearchMatch;
        try {
          ListChapters(sm);
          RefreshVerses(sm);
        } catch (Exception E) {
          TraceLogger.Send(E, "On Book Selected");
        }
        if (this.CChapters.Items.Count > 0) {
          this.CChapters.SelectedItem = this.CChapters.Items[0];
        }
        if (this.CVerses.Items.Count > 0) {
          this.CVerses.SelectedItem = this.CVerses.Items[0];
        }
      }
      this.refreshChapt = false;
      this.refreshVerses = false;
    }

    public bool ListChapters(BibleSearchMatch sm) {
      this.CChapters.Items.Clear();
      if (sm == null) return false;
      BibleBook bk = null;
      if (!BibleStore.BibleStore.EngBible.Books.TryGetValue(sm.BookId.ToLower(), out bk)) return false;
      for (var cn = 0; cn < bk.Chapters.Count; cn++) {
        var res = BibleStore.BibleStore.FindEn(string.Format($"{sm.BookName} {(cn+1).ToString()}"));
        if (res.Matches.Length < 1) continue;
        var match = res.Matches[0];
        this.CChapters.Items.Add(CreateItem(match, match.Verses.First()));
      }
      this.CChapters.SelectedItem = null;
      return false;
    }

    public bool RefreshChapters(BibleSearchMatch match) {
      this.CChapters.Items.Clear();
      this.CChapters.Items.Add(CreateItem(match, match.Verses.First()));
      return false;
    }
    
    private void OnChapterSelected(object sender, SelectionChangedEventArgs e) {
      if (this.refreshVerses) return;
      this.refreshVerses = true;
      this.CVerses.Items.Clear();
      if (e.AddedItems.Count > 0) {
        var item = e.AddedItems[0] as ListBoxItem;
        var sm = item.Tag as BibleSearchMatch;
        try {
          RefreshVerses(sm);
        } catch (Exception E) {
          TraceLogger.Send(E, "On Chapter Selected");
        }
        if (this.CVerses.Items.Count > 0) {
          this.CVerses.SelectedItem = this.CVerses.Items[0];
        }
      }
      this.refreshVerses = false;
    }

    public bool RefreshVerses(BibleSearchMatch match) {
      this.CVerses.Items.Clear();
      foreach (var vs in match.Verses) {
        this.CVerses.Items.Add(CreateItem(match, vs));
      }
      return false;
    }
    
    private void OnVerseSelected(object sender, SelectionChangedEventArgs e) {
      if (e.AddedItems.Count > 0) {
        var item = e.AddedItems[0] as ListBoxItem;
        var sm = item.Tag as BibleSearchMatch;
        try {
          ShowEntry(sm);
        } catch (Exception E) {
          TraceLogger.Send(E, "On Chapter Selected");
        }
      }
    }

    private void SearchBible(string query){
      this.CSearchResults.Items.Clear();
      var res = BibleStore.BibleStore.FindEn(query);
      if (res == null || res.Matches == null || res.Matches.Length < 1) return;
      foreach (var item in res.Matches.Select(x => CreateItem(x, x.Verses.First()))){
        this.CSearchResults.Items.Add(item);
      }
      this.PnlSearch.Visibility = Visibility.Visible;
    }
    
    public VerseEntry CreateEntry(BibleSearchMatch match, BibleSearchVerse verse) {
      VerseEntry verseEntry = new VerseEntry();
      string name = string.Format("{0} {1} {2}:{3}", match.BookNames["zh"], match.BookNames["en"], match.Chapter.ToString(), verse.Verse.ToString());
      //verseEntry.Search = string.Format("{0} {1}:{2}", (object) book.Name, (object) chp, (object) verse);
      verseEntry.SetContent(name, verse.Content["en"], verse.Content["zh"]);
      return verseEntry;
    }
    
    private void ShowDisplay() {
      try {
        if (!this.mDisplay.IsVisible) {
          if (!this.mInitDisplay) {
            PropertyStore.LoadMetrics((Window) this.mDisplay);
            this.mInitDisplay = true;
            if (this.mDisplay.WindowState == WindowState.Maximized) {
              this.mDisplay.mMaximize = true;
              this.mDisplay.WindowState = WindowState.Normal;
            }
            this.mDisplay.ShowActivated = true;
            this.mDisplay.Show();
            if (this.mDisplay.mMaximize) this.mDisplay.WindowState = WindowState.Maximized;
          } else {
            this.mDisplay.ShowActivated = true;
            this.mDisplay.Show();
          }
        } else {
          if (this.mDisplay.WindowState != WindowState.Minimized) return;
          if (this.mDisplay.mMaximize)
            this.mDisplay.WindowState = WindowState.Maximized;
        }
        TraceLogger.Send("Show Display Self Adjust");
        this.mDisplay.SelfAdjust();
      } catch (System.Exception ex) {
        TraceLogger.Send(ex, "Unable to activate");
      }
    }

    private void OnIncFontClick(object sender, RoutedEventArgs e) => this.mDisplay.IncFontSize();
    private void OnDecFontClick(object sender, RoutedEventArgs e) => this.mDisplay.DecFontSize();
    private void OnAdjFontClick(object sender, RoutedEventArgs e)  => this.mDisplay.SelfAdjust(true);

    private void OnCloseDispClick(object sender, RoutedEventArgs e) {
      if (this.mDisplay.IsVisible)this.mDisplay.CloseHide();
    }

    private void OnShowDispClick(object sender, RoutedEventArgs e) => this.ShowDisplay();

    private void OnSearchDispClick(object sender, RoutedEventArgs e) {
      if (this.CmdSearch.Content.ToString() == "Search") {
        this.PnlContents.Visibility = Visibility.Collapsed;
        this.PnlSearch.Visibility = Visibility.Visible;
        //this.CSearch.Focus();
        this.CmdSearch.Content = (object) "Bible";
        this.CTitle.Text = "Enter Search Term";
      } else {
        this.PnlContents.Visibility = Visibility.Visible;
        this.PnlSearch.Visibility = Visibility.Collapsed;
        //this.CEntry.Focus();
        this.CmdSearch.Content = (object) "Search";
        this.CTitle.Text = "Enter Bible Location";
      }
    }

    private void OnMainKeyDown(object sender, KeyEventArgs e) {
      if (e.Key == Key.Escape) {
        this.Close();
      }
    }

    private void OnEntryKeyDown(object sender, KeyEventArgs e) {
      if (e.Key == Key.Enter) {
        this.CBooks.Focus();
      }
    }

    private void OnNamesKeyDown(object sender, KeyEventArgs e) {
      if (e.Key == Key.OemTilde) {
        string lang = CNames.Tag as string;
        if (lang.IsNotNullOrEmpty() && lang == "en")
          LoadNames(BibleStore.BibleStore.ChiBible);
        else LoadNames(BibleStore.BibleStore.EngBible);
      } else if (e.Key == Key.Left || e.Key == Key.Back) {
        this.CEntry.Focus();
      } else if (e.Key == Key.Right || e.Key == Key.Enter) {
        this.CBooks.Focus();
        if (this.CBooks.SelectedItem != null) {
          var li = this.CBooks.SelectedItem as ListBoxItem;
          li.Focus();
        }
      }
    }

    private void OnBookKeyDown(object sender, KeyEventArgs e) {
      if (e.Key == Key.D) {
        this.CEntry.Focus();
        e.Handled = true;
      } else if (e.Key == Key.Left || e.Key == Key.Back) {
        this.CNames.Focus();
        if (this.CNames.SelectedItem != null) {
          var li = this.CNames.SelectedItem as ListBoxItem;
          li.Focus();
        }
      } else if (e.Key == Key.Enter) {
        this.CVerses.Focus();
        if (this.CVerses.SelectedItem != null) {
          var li = this.CVerses.SelectedItem as ListBoxItem;
          li.Focus();
        }
      } else if (e.Key == Key.Right) {
        this.CChapters.Focus();
        if (this.CChapters.SelectedItem != null) {
          var li = this.CChapters.SelectedItem as ListBoxItem;
          li.Focus();
        }
      }
    }

    private void OnChapterKeyDown(object sender, KeyEventArgs e) {
      if (e.Key == Key.D) {
        this.CEntry.Focus();
        e.Handled = true;
      } else if (e.Key == Key.Left || e.Key == Key.Back) {
        this.CBooks.Focus();
        if (this.CBooks.SelectedItem != null) {
          var li = this.CBooks.SelectedItem as ListBoxItem;
          li.Focus();
        }
      } else if (e.Key == Key.Right || e.Key == Key.Enter) {
        this.CVerses.Focus();
        if (this.CVerses.SelectedItem != null) {
          var li = this.CVerses.SelectedItem as ListBoxItem;
          try {
            if(li!=null)li.Focus();
          } catch (Exception) {}
        }
      }
    }

    private void OnVersesKeyDown(object sender, KeyEventArgs e) {
      if (e.Key == Key.D) {
        this.CEntry.Focus();
        e.Handled = true;
      } else if (e.Key == Key.Left || e.Key == Key.Back) {
        if (this.CChapters.SelectedItem != null) {
          var li = this.CChapters.SelectedItem as ListBoxItem;
          li.Focus();
        }else this.CChapters.Focus();
      }else if (e.Key == Key.Right || e.Key == Key.Enter) {
        try {
          if (this.mDisplay.IsVisible) return;
          this.ShowDisplay();
        } catch (System.Exception ex) {
          TraceLogger.Send(ex, "CName Selected");
        }
      }
    }

    private void OnVerseDblClick(object sender, MouseButtonEventArgs e) {
      try {
        if (this.mDisplay.IsVisible) return;
        this.ShowDisplay();
      } catch (System.Exception ex) {
        TraceLogger.Send(ex, "CName Selected");
      }
    }

    private void OnSearchDblClick(object sender, MouseButtonEventArgs e) {
      try {
        ListBoxItem selectedItem = this.CSearchResults.SelectedItem as ListBoxItem;
        /*
        this.mBook = selectedItem.Tag as BibleBook;
        VerseEntry content = selectedItem.Content as VerseEntry;
        this.mChapter = content.Chapter;
        this.mVerse = content.Verse;
        this.PnlContents.Visibility = Visibility.Visible;
        this.PnlSearch.Visibility = Visibility.Collapsed;
        this.CEntry.Text = string.Format("{0} {1}:{2}", (object) this.mBook.Name, (object) this.mChapter, (object) this.mVerse);
        this.CEntry.Focus();
        this.CmdSearch.Content = (object) "Search";
        this.CTitle.Text = "Enter Bible Location";
        this.UpdateChpEntries();
        this.UpdateVersesEntries();
        this.ShowEntry(content);
        */
      } catch (System.Exception ex) {
        TraceLogger.Send(ex, "CName Selected");
      }
    }
    
    private void OnFocus(object sender, RoutedEventArgs e)  => this.CEntry.SelectAll();

    private void OnSearchSelected(object sender, SelectionChangedEventArgs e) {
    }

    private void OnNameKeyDown(object sender, KeyEventArgs e) {
    }

    private void OnGridLoaded(object sender, RoutedEventArgs e) {
    }

    private void ShowEntry(BibleSearchMatch ve) => this.mDisplay.SetContent(ve);

    private void LoadNames(Bible bible) {
      this.CNames.Tag = bible.Lang;
      this.CNames.Items.Clear();
      var cb = BibleStore.BibleStore.BookIds.Select(x => bible.Books[x.ToLower()]);
      foreach (var item in cb.Select(x => new ListBoxItem() {Content = x.Name, Tag = x}))
        this.CNames.Items.Add(item);
    }

    protected override void OnClosed(EventArgs e) {
      if (this.mDisplay.IsVisible) {
        PropertyStore.StoreMetrics((Window) this.mDisplay);
      }
      this.mDisplay.CloseWindow = true;
      this.mDisplay.Close();
      PropertyStore.StoreMetrics((Window) this);
      base.OnClosed(e);
      BibleLive.App.Current.Shutdown(0);
    }

    private string mSearchString { get; set; }
    private bool mInitDisplay = false;
    private bool refreshing = false;
    private bool refreshChapt = false;
    private bool refreshVerses = false;
    private BibleSearchResults searchReesults = null;
  }
}
