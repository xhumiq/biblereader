﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using BibleStore;
using MCL.Logging;

namespace BibleLive {
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application {
    public static Bible EngBible { get; private set; } 
    public static Bible ChiBible { get; private set; }
    protected override void OnStartup(StartupEventArgs e) {
      base.OnStartup(e);
      BibleStore.BibleStore.LoadNames("en","data/en_names.yml");
      BibleStore.BibleStore.LoadNames("zh","data/zh_names.yml");
      ChiBible = BibleStore.BibleStore.LoadOSIS("data/Chi-Union.xml");
      EngBible = BibleStore.BibleStore.LoadOSIS("data/NKJV.xml");
    }

    protected override void OnExit(ExitEventArgs e) {
      base.OnExit(e);
      TraceLogger.Send("On Exit");
    }

    private void OnUnhandledEvent(object sender, DispatcherUnhandledExceptionEventArgs e) => TraceLogger.Send(e.Exception, "Unhandled Event");
  }
}
