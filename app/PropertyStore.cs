﻿using MCL.Configuration;
using Microsoft.Win32;
using System.Windows;
using MCL.Logging;

namespace BibleLive {
  public class PropertyStore
  {
    public static void StoreMetrics(Window window){
      TraceLogger.Send($"Key: {"BibleLive\\" + window.Name + "\\Metrics"}");
      RegistryKey softwareKey = RegistryDS.CreateSoftwareKey(Registry.CurrentUser, "Omega Business Consulting", "BibleLive\\" + window.Name + "\\Metrics");
      RegistryDS.SetValue(softwareKey, "Left", RegistryValueKind.DWord, true, (object) window.Left);
      RegistryDS.SetValue(softwareKey, "Top", RegistryValueKind.DWord, true, (object) window.Top);
      RegistryDS.SetValue(softwareKey, "Width", RegistryValueKind.DWord, true, (object) window.Width);
      RegistryDS.SetValue(softwareKey, "Height", RegistryValueKind.DWord, true, (object) window.Height);
      RegistryDS.SetValue(softwareKey, "WindowState", RegistryValueKind.String, true, (object) window.WindowState);
    }

    public static void LoadMetrics(Window window){
      RegistryKey softwareKey = RegistryDS.CreateSoftwareKey(Registry.CurrentUser, "Omega Business Consulting", "BibleLive\\" + window.Name + "\\Metrics");
      if (softwareKey == null)
        return;
      int num1 = RegistryDS.GetInt(softwareKey, "Left");
      if (num1 > 0)
        window.Left = (double) num1;
      int num2;
      if ((num2 = RegistryDS.GetInt(softwareKey, "Top")) > 0)
        window.Top = (double) num2;
      int num3;
      if ((num3 = RegistryDS.GetInt(softwareKey, "Width")) > 0)
        window.Width = (double) num3;
      int num4;
      if ((num4 = RegistryDS.GetInt(softwareKey, "Height")) > 0)
        window.Height = (double) num4;
      if (!(RegistryDS.GetString(softwareKey, "WindowState") == "Maximized"))
        return;
      window.WindowState = WindowState.Maximized;
    }
  }
}
