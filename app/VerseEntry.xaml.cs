﻿using System.Windows.Controls;

namespace BibleLive {
  public partial class VerseEntry : UserControl {
    public VerseEntry() {
      InitializeComponent();
    }
    public void SetContent(string name, string english, string chinese){
      this.CName.Content = (object) name;
      this.CText.Text = english;
      this.CText2.Text = chinese;
    }
    public string Source => this.CName.Content as string;
    public string EnglishText => this.CText.Text;
    public string ChineseText => this.CText2.Text;
  }
}

