﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MCL;
using MCL.Configuration;
using MCL.Data.Xml;
using MCL.ErrorHandling;
using MCL.Logging;
using MCL.Net;
using MCL.Threading;
using MCL.Service.NTService;

using BibleStore;

namespace BibleCli {
  class Program {
    static void Main(string[] args) {
      CommandLine.ExecuteAndFallback<Program>(() => {
        return 0;
      });
    }
    public static void Load_Names(CommandLine cmd) {
      cmd.AddAliases("names", "en");
      cmd.Run(a => {
        foreach (var n in BibleStore.BibleStore.ChiBookNames) {
          var ma = BibleStore.BibleStore.RE_CHVERSE.Match(n.Value);
          if (ma != null) {
            var nm = new BibleName() {
              Id = n.Key,
              Volume = ma.Groups[1].Value.Trim().ToIntOrZero(),
              Suffix = ma.Groups[2].Value.TrimNullIfEmpty(),
              Full = n.Value,
            };
          }
        }
        return 0;
      });
    }
    public static void Load_File(CommandLine cmd) {
      cmd.AddAliases("load", "import");
      cmd.Run(a => {
        BibleStore.BibleStore.LoadNames("en","data/en_names.yml");
        BibleStore.BibleStore.LoadNames("zh","data/zh_names.yml");
        BibleStore.BibleStore.LoadOSIS("data/Chi-Union.xml");
        BibleStore.BibleStore.LoadOSIS("data/NKJV.xml");
        var r = BibleStore.BibleStore.FindEn("j 1:2");
        foreach (var m in r.Matches) {
          Console.WriteLine($"Match: {m.BookName} {m.BookNames.Count.ToString()} {m.Chapter.ToString()}");
          foreach (var v in m.Verses) {
            Console.WriteLine($"{v.Verse.ToString()}:");
            foreach (var c in v.Content) {
              Console.WriteLine($"  {c.Key}: {c.Value}");
            }
          }
        }
        return 0;
      });
    }
  }
}
