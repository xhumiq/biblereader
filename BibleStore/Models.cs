﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibleStore {
	public class Bible {
		public string Lang { get; set; }
		public string Name { get; set; }
		public SortedDictionary<string, BibleBook> Books { get; set; } = new SortedDictionary<string, BibleBook>();
	}

	public class BibleBook {
		public string Id { get; set; }
		public string Name { get; set; }
		public string LCName { get; set; }
		public SortedDictionary<int, BibleChapter> Chapters { get; set; } = new SortedDictionary<int, BibleChapter>();
	}

	public class BibleChapter {
		public BibleBook Book { get; set; }
		public SortedDictionary<int, string> Verses { get; set; } = new SortedDictionary<int, string>();
		public int Number { get; set; }
	}

	public class BibleSearchResults {
		public BibleIndexRequest Request  { get; set; }
		public BibleSearchMatch[] Matches { get; set; }
	}

	public class BibleSearchMatch {
		public string BookId { get; set; }
		public string BookName { get; set; }
		public SortedDictionary<string, string> BookNames { get; set; } = new SortedDictionary<string, string>();
		public int Chapter { get; set; }
		public BibleSearchVerse[] Verses { get; set; }
		public BibleSearchVerse SelectedVerse{ get; set; }

		public BibleSearchMatch Clone(BibleSearchVerse verse = null) {
			return new BibleSearchMatch() {
				BookId = this.BookId,
				BookName =  this.BookName,
				BookNames =  this.BookNames,
				Chapter =  this.Chapter,
				Verses = this.Verses,
				SelectedVerse = verse,
			};
		}
	}

	public class BibleSearchVerse {
		public int Verse { get; set; }
		public SortedDictionary<string, string> Content { get; set; } = new SortedDictionary<string, string>();
	}

	public class BibleIndexRequest {
		public string SearchText { get; set; }
		public int Volume { get; set; }
		public string Prefix { get; set; }
		public int Chapter { get; set; }
		public int Verse { get; set; }
		public int Range { get; set; }
	}

	public class BibleName {
		public string Id { get; set; }
		public int Volume { get; set; }
		public string Suffix { get; set; }
		public string Full { get; set; }
	}
}
