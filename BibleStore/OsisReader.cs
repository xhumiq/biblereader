﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MCL;
using MCL.Data.IO;
using MCL.ErrorHandling;

namespace BibleStore {
  public class OsisReader {
    public OsisReader() {
    }
    public Bible ReadOSIS(string fname) {
      fname = FileIO.GetFileName(fname, App.RootPath);
      using (Stream stream = (Stream)File.OpenRead(fname)) {
        return this.ReadOSIS(stream);
      }
    }
    public Bible ReadOSIS(Stream stream) => this.ReadOSIS(new StreamReader(stream));
    public Bible ReadOSIS(StreamReader reader) => this.ReadOSIS(new XmlStreamer((TextReader)reader));
    public Bible ReadOSIS(XmlStreamer streamer) {
      var bible = new Bible { };
      streamer.ReadElement("osis/osisText", (x, n) => {
        x.ReadElement("header/work", (xh, nh) => {
          bible.Name = x.ReadText("title", null).Trim();
          bible.Lang = x.ReadText("language", null).Trim();
        });
        x.ReadElement("div", (xd, nd) => {
          var book = new BibleBook { Id = nd.GetAttr("osisID") };
          var names = BibleStore.BookNames[bible.Lang];
          book.Name = names[book.Id.ToLower()];
          book.LCName = book.Name.ToLower().Trim(); 
          bible.Books[book.Id.ToLower()] = book;
          var pc = 0;
          x.ReadElement("chapter", (xc, nc) => {
            var oid = nc.GetAttr("osisID");
            if (!oid.StartsWith(book.Id)) throw new SysException("OsisID is invalid for chapter in {0} - {1}", book.Id, oid);
            var oidn = oid.Split('.');
            if (oidn.Length != 2) throw new SysException("OsisID is invalid for chapter in {0} - {1}", book.Id, oid);
            var cn = oidn[1].ToIntOrZero();
            if (cn != pc + 1) throw new SysException("OsisID is invalid for chapter in {0} Prev Chapt: {1} - {2}", book.Id, pc, oid);
            pc++;
            var chapt = new BibleChapter{
              Book = book,
              Number = cn,
            };
            var pv = 0;
            x.ReadTexts("verse", (xv, nv) => {
              oid = nv.GetAttr("osisID");
              if (oid.IsNullOrEmpty() || !oid.StartsWith(book.Id)) throw new SysException("OsisID is invalid for verse in {0} {1} - {2}", book.Id, chapt.Number, oid);
              oidn = oid.Split('.');
              if (oidn.Length != 3) throw new SysException("OsisID is invalid for verse in {0} {1} - {2}", book.Id, chapt.Number, oid);
              cn = oidn[1].ToIntOrZero();
              if (cn != chapt.Number) throw new SysException("OsisID is invalid for verse in {0} {1} - {2}", book.Id, chapt.Number, oid);
              var vn = oidn[2].ToIntOrZero();
              if (vn != pv + 1) throw new SysException("OsisID is invalid for verse in {0} {1} Prev Verse {2} - {3}", book.Id, chapt.Number, pv, oid);
              var text = nv.Text?.TrimNullIfEmpty();
              if (text.IsNullOrEmpty())throw new SysException("Text not found in Verse {0} {1}:{2} - {3}", book.Id, chapt.Number, vn, oid);
              chapt.Verses[vn] = text;
              pv++;
            });
            if(chapt.Verses.Count < 1)throw new SysException("No verses found for chapter {0} {1}", book.Id, chapt.Number);
            book.Chapters[chapt.Number] = chapt;
          });
          if (bible.Books.Count < 1)throw new SysException("No chapters found for book {0} {1}", bible.Name, book.Name);
        });
      });
      return bible;
    }
  }
  public class XmlStreamer: XmlTextReader {
    public int CurDepth { get; private set; }
    private Stack<XmlStreamNode> Stack { get; } = new Stack<XmlStreamNode>();
    public XmlStreamer(TextReader reader) : base(reader) { }
    public void ReadElement(string path, Action<XmlStreamer, XmlStreamNode> scan) {
      var name = path;
      var nms = path.Split('/');
      if (nms.Length > 1) {
        name = nms[0];
        path = path.Substring(name.Length + 1);
      } else path = "";
      var readDepth = this.CurDepth + 1;
      while (this.CurDepth >= (readDepth -1)) {
        if (!this.Read()) break;
        switch (this.NodeType) {
          case XmlNodeType.Element: {
            if (this.IsEmptyElement) continue;
            this.CurDepth++;
            var elem = CreateNode();
            this.Stack.Push(elem);
            if (readDepth == this.CurDepth && name == this.Name) {
              if (path != "") {
                this.ReadElement(path, scan);
              } else if (scan!=null) scan(this, elem);
            }
          }break;
          case XmlNodeType.EndElement: {
            var elem = this.Stack.Pop();
            if (this.Name != elem.Name)
              throw new SysException("End Elem: {0} {1}", elem.Name, this.Name);
            this.CurDepth--;
            if (elem.Name == name) {
              if (this.CurDepth <= (readDepth - 1)) return;
            }
          }
          break;
        }
      }
    }
    public string ReadText(string path, Action<XmlStreamer, XmlStreamNode> scan) {
      var name = path;
      var nms = path.Split('/');
      var readDepth = this.CurDepth + 1;
      if (nms.Length > 1) {
        name = nms[0];
        path = path.Substring(name.Length + 1);
      } else path = "";
      string totBody = "", body = "";
      XmlStreamNode lastElem = null;
      while (this.CurDepth >= (readDepth -1)) {
        if (!this.Read()) break;
        switch (this.NodeType) {
          case XmlNodeType.Element: {
            if (this.IsEmptyElement) continue;
            this.CurDepth++;
            var elem = CreateNode();
            this.Stack.Push(elem);
            if (readDepth == this.CurDepth && name == this.Name) {
              if (path != "") {
                if (totBody.Length > 0) totBody += "\r\n";
                totBody += this.ReadText(path, null);
              } else lastElem = elem;
            } else lastElem = null;
          }break;
          case XmlNodeType.EndElement: {
            var elem = this.Stack.Pop();
            if (this.Name != elem.Name)
              throw new SysException("End Elem: {0} {1}", elem.Name, this.Name);
            this.CurDepth--;
            if (elem.Name == name) {
              if (totBody.Length > 0) totBody += "\r\n";
              totBody += body;
              elem.Text = body;
              if (scan != null)scan(this, elem);
              body = "";
              if (this.CurDepth <= (readDepth - 1)) return totBody;
            }
            body = "";
          }break;
          case XmlNodeType.CDATA:
          case XmlNodeType.Text: {
            if (lastElem != null) {
              if (body.Length > 0) body += "\r\n";
              body += this.Value;
            }
          }break;
        }
      }
      return totBody;
    }
    public string ReadTexts(string path, Action<XmlStreamer, XmlStreamNode> scan) {
      var name = path;
      var nms = path.Split('/');
      var readDepth = this.CurDepth + 1;
      if (nms.Length > 1) {
        name = nms[0];
        path = path.Substring(name.Length + 1);
      } else path = "";
      string totBody = "", body = "";
      XmlStreamNode lastElem = null;
      while (this.CurDepth >= (readDepth -1)) {
        if (!this.Read()) break;
        switch (this.NodeType) {
          case XmlNodeType.Element: {
            if (this.IsEmptyElement) continue;
            this.CurDepth++;
            var elem = CreateNode();
            this.Stack.Push(elem);
            if (readDepth == this.CurDepth && name == this.Name) {
              if (path != "") {
                if (totBody.Length > 0) totBody += "\r\n";
                totBody += this.ReadText(path, null);
              } else lastElem = elem;
            } else lastElem = null;
          }break;
          case XmlNodeType.EndElement: {
            var elem = this.Stack.Pop();
            if (this.Name != elem.Name)
              throw new SysException("End Elem: {0} {1}", elem.Name, this.Name);
            this.CurDepth--;
            if (elem.Name == name) {
              if (totBody.Length > 0) totBody += "\r\n";
              totBody += body;
              elem.Text = body;
              if (scan != null)scan(this, elem);
              body = "";
            }
            body = "";
          }break;
          case XmlNodeType.CDATA:
          case XmlNodeType.Text: {
            if (lastElem != null) {
              if (body.Length > 0) body += "\r\n";
              body += this.Value;
            }
          }break;
        }
      }
      return totBody;
    }
    private XmlStreamNode CreateNode() {
      var node = new XmlStreamNode {
        Name = this.Name,
        Depth = this.CurDepth,
      };
      for (int attInd = 0; attInd < this.AttributeCount; attInd++) {
        this.MoveToAttribute(attInd);
        node.Attrib[this.Name] = this.Value;
      }
      this.MoveToElement();
      return node;
    }
  }
  public class XmlStreamNode {
    public string Name { get; set; }
    public int Depth { get; set; }
    public SortedDictionary<string, string> Attrib { get; } = new SortedDictionary<string, string>();
    public string Text { get; set; }
    public string GetAttr(string name) {
      var value = "";
      if (this.Attrib.TryGetValue(name, out value)) {
        return value;
      }
      return null;
    }
  }
}
