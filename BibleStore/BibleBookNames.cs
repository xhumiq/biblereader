﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MCL;
using MCL.Data.IO;
using MCL.Data.Legacy;
using MCL.ErrorHandling;

namespace BibleStore {
  public static class BibleStore {
		static BibleStore() {}
		public readonly static SortedDictionary<string, Bible> Bibles = new SortedDictionary<string, Bible>();
		public static Bible LoadOSIS(string fname) {
			var rd = new OsisReader();
			var bible = rd.ReadOSIS(fname);
			Bibles[bible.Lang] = bible;
			return bible;
		}

		public static BibleSearchResults FindEn(string text) {
			var req = ParseRequest(text);
			return FindEn(req);
		}

		public static BibleSearchResults FindEn(BibleIndexRequest req) {
			var res = new List<BibleSearchMatch>();
			var bres = new BibleSearchResults() { Request = req, Matches = res.ToArray() };
			if (req.Prefix.IsNullOrEmpty()) return bres;
			BibleSearchVerse vs;
			foreach (var n in EngBookNames.Values) {
				if (!n.Suffix.ToLower().StartsWith(req.Prefix)) continue;
				if (req.Volume > 0 && n.Volume != req.Volume) continue;
				var bk = EngBible.Books[n.Id];
				if (req.Chapter > bk.Chapters.Count) req.Chapter = bk.Chapters.Count;
				else if (req.Chapter < 1) req.Chapter = 1;
				var sr = new BibleSearchMatch() {
					BookId = bk.Id,
					BookName = bk.Name,
					BookNames = new SortedDictionary<string, string>(),
					Chapter = req.Chapter,
				};
				var vss = new SortedDictionary<int, BibleSearchVerse>();
				int v = req.Verse, r = req.Range;
				var vl = new List<BibleSearchVerse>();
				foreach (var b in Bibles.Values) {
					bk = b.Books[sr.BookId.ToLower()];
					sr.BookNames[b.Lang] = bk.Name;
					var ch = bk.Chapters[sr.Chapter];
					if (v > ch.Verses.Count) v = ch.Verses.Count;
					else if (v < 1) { 
						v = 1;
						r = ch.Verses.Count;
					}
					if (r < v) r = v;
					else if (r > ch.Verses.Count) r = ch.Verses.Count;
					string txt = null;
					for (var idx = v; idx <= r; idx++) {
						if (!ch.Verses.TryGetValue(idx, out txt)) continue;
						if (txt.IsNullOrEmpty()) continue;
						if(!vss.TryGetValue(idx, out vs)){
							vs = new BibleSearchVerse() {
								Verse = idx,
								Content = new SortedDictionary<string, string>(),
							};
							vss[idx] = vs;
							vl.Add(vs);
						}
						vs.Content[b.Lang] = txt;
					}
				}
				if (vl.Count < 1) continue;
				sr.Verses = vl.ToArray();
				res.Add(sr);
			}
			bres.Matches = res.ToArray();
			return bres;
		}

		public static BibleIndexRequest ParseRequest(string text) {
			var ma = RE_CHVERSE.Match(text);
			var req = new BibleIndexRequest() { SearchText = text };
			if (ma == null) return req;
			req.Volume = ma.Groups[1].Value.ToIntOrZero();
			req.Prefix = ma.Groups[2].Value.ToLower().Trim();
			req.Chapter = ma.Groups[3].Value.ToIntOrZero();
			req.Verse = ma.Groups[4].Value.ToIntOrZero();
			req.Range = ma.Groups[5].Value.ToIntOrZero();
			if (req.Chapter < 1) req.Chapter = 1;
			return req;
		}

		public static SortedDictionary<string, string> LoadNames(string lang, string path) {
			var sd = new SortedDictionary<string, string>();
			path = FileIO.GetFileName(path, App.RootPath);
			var file = File.ReadAllText(path);
			if (lang == "en")EngBookNames.Clear();
			foreach (var fl in file.Split('\n').Select(x => x.TrimNullIfEmpty())) {
				if (fl == null)continue;
				var fn = fl.Split(':').Select(x => x.TrimNullIfEmpty()).Where(x=>x.IsNotNullOrEmpty()).ToArray();
				if(fn.Length != 2)throw new SysException($"File {path} contains bad line: {fl}");
				sd[fn[0]] = fn[1];
				if (lang == "en") {
					var ma = RE_CHVERSE.Match(fn[1]);
					if (ma != null) {
						var nm = new BibleName() {
							Id = fn[0],
							Volume = ma.Groups[1].Value.ToIntOrZero(),
							Suffix = ma.Groups[2].Value.TrimNullIfEmpty(),
							Full = fn[1],
						};
						EngBookNames[nm.Id] = nm;
					}
				}
			}
			BookNames[lang] = sd;
			return sd;
		}

		public static Bible EngBible => Bibles["en"];
		public static Bible ChiBible => Bibles["zh"];
		public readonly static Regex RE_CHVERSE =
			new Regex(@"^(\d{0,3})\s*([A-Za-z]+)\s*(?:(\d{1,4})(?:[ :.]+(\d{1,4})(?:[ :.-]+(\d{1,4}))?)?)?",
				RegexOptions.Compiled | RegexOptions.IgnoreCase);
		public readonly static SortedDictionary<string, SortedDictionary<string, string>> BookNames = new SortedDictionary<string, SortedDictionary<string, string>>();
		public readonly static SortedDictionary<string, BibleName> EngBookNames = new SortedDictionary<string, BibleName>();
		public static SortedDictionary<string, string> ChiBookNames => BookNames["zh"];
		public readonly static string[] BookIds = {
			"Gen",
			"Exod",
			"Lev",
			"Num",
			"Deut",
			"Josh",
			"Judg",
			"Ruth",
			"1Sam",
			"2Sam",
			"1Kgs",
			"2Kgs",
			"1Chr",
			"2Chr",
			"Ezra",
			"Neh",
			"Esth",
			"Job",
			"Ps",
			"Prov",
			"Eccl",
			"Song",
			"Isa",
			"Jer",
			"Lam",
			"Ezek",
			"Dan",
			"Hos",
			"Joel",
			"Amos",
			"Obad",
			"Jonah",
			"Mic",
			"Nah",
			"Hab",
			"Zeph",
			"Hag",
			"Zech",
			"Mal",
			"Matt",
			"Mark",
			"Luke",
			"John",
			"Acts",
			"Rom",
			"1Cor",
			"2Cor",
			"Gal",
			"Eph",
			"Phil",
			"Col",
			"1Thess",
			"2Thess",
			"1Tim",
			"2Tim",
			"Titus",
			"Phlm",
			"Heb",
			"Jas",
			"1Pet",
			"2Pet",
			"1John",
			"2John",
			"3John",
			"Jude",
			"Rev",
		};
		public readonly static string[,] OTNames = {
			{"Gen","Genesis","創 世 紀"},
			{"Exod","Exodus","出 埃 及 記"},
			{"Lev","Leviticus","利 未 記"},
			{"Num","Numbers","民 數 記"},
			{"Deut","Deuteronomy","申 命 記"},
			{"Josh","Joshua","約 書 亞 記"},
			{"Judg","Judges","士 師 記"},
			{"Ruth","Ruth","路 得 記"},
			{"1Sam","1 Samuel","撒 母 耳 記 上"},
			{"2Sam","2 Samuel","撒 母 耳 記 下"},
			{"1Kgs","1 Kings","列 王 記 上"},
			{"2Kgs","2 Kings","列 王 記 下"},
			{"1Chr","1 Chronicles","歷 代 志 上"},
			{"2Chr","2 Chronicles","歷 代 志 下"},
			{"Ezra","Ezra","以 斯 拉 記"},
			{"Neh","Nehemiah","尼 希 米 記"},
			{"Esth","Esther","以 斯 帖 記"},
			{"Job","Job","約 伯 記"},
			{"Ps","Psalms","詩 篇"},
			{"Prov","Proverbs","箴 言"},
			{"Eccl","Ecclesiastes","傳 道 書"},
			{"Song","Song of Solomon","雅 歌"},
			{"Isa","Isaiah","以 賽 亞 書"},
			{"Jer","Jeremiah","耶 利 米 書"},
			{"Lam","Lamentations","耶 利 米 哀 歌"},
			{"Ezek","Ezekiel","以 西 結 書"},
			{"Dan","Daniel","但 以 理 書"},
			{"Hos","Hosea","何 西 阿 書"},
			{"Joel","Joel","約 珥 書"},
			{"Amos","Amos","阿 摩 司 書"},
			{"Obad","Obadiah","俄 巴 底 亞 書"},
			{"Jonah","Jonah","約 拿 書"},
			{"Mic","Micah","彌 迦 書"},
			{"Nah","Nahum","那 鴻 書"},
			{"Hab","Habakkuk","哈 巴 谷 書"},
			{"Zeph","Zephaniah","西 番 雅 書"},
			{"Hag","Haggai","哈 該 書"},
			{"Zech","Zechariah","撒 迦 利 亞"},
			{"Mal","Malachi","瑪 拉 基 書"},
		};
		public static string[,] NTNames = {
			{"Matt","Matthew","馬 太 福 音"},
			{"Mark","Mark","馬 可 福 音"},
			{"Luke","Luke","路 加 福 音"},
			{"John","John","約 翰 福 音"},
			{"Acts","Acts","使 徒 行 傳"},
			{"Rom","Romans","羅 馬 書"},
			{"1Cor","1 Corinthians","歌 林 多 前 書"},
			{"2Cor","2 Corinthians","歌 林 多 後 書"},
			{"Gal","Galatians","加 拉 太 書"},
			{"Eph","Ephesians","以 弗 所 書"},
			{"Phil","Philippians","腓 利 比 書"},
			{"Col","Colossians","歌 羅 西 書"},
			{"1Thess","1 Thessalonians","帖 撒 羅 尼 迦 前 書"},
			{"2Thess","2 Thessalonians","帖 撒 羅 尼 迦 後 書"},
			{"1Tim","1 Timothy","提 摩 太 前 書"},
			{"2Tim","2 Timothy","提 摩 太 後 書"},
			{"Titus","Titus","提 多 書"},
			{"Phlm","Philemon","腓 利 門 書"},
			{"Heb","Hebrews","希 伯 來 書"},
			{"Jas","James","雅 各 書"},
			{"1Pet","1 Peter","彼 得 前 書"},
			{"2Pet","2 Peter","彼 得 後 書"},
			{"1John","1 John","約 翰 一 書"},
			{"2John","2 John","約 翰 二 書"},
			{"3John","3 John","約 翰 三 書"},
			{"Jude","Jude","猶 大 書"},
			{"Rev","Revelation","啟 示 錄"},
		};
	}
}
